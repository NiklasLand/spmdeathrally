﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiPlayerHook : MonoBehaviour {

    UIManager Gui;
    PlayerController player;
    void Start ()
    {
        Gui = GetComponent<UIManager>();
    }
	

	void Update ()
    {
        player = PlayerController.LocalVersion;
        if (player == null)
            return;
        Gui.SetPlayerHealthBar((int)player.Car.CurrenHealth ,player.Car.MaxHealth);
        Gui.SetPlayerCurrentLap(player.currentLap+1);
        Gui.SetPlayerMoney(player.Money);
        Gui.SetPlayerPlacement(GameManager.instance.GetMyPlacement(player.Id));
        Gui.SetMaxLaps(3);
        //Gui.UpdatePlayerAmmo();
    }
}
