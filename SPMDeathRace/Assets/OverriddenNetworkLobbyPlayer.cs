﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class OverriddenNetworkLobbyPlayer : NetworkLobbyPlayer {

    CarSelection carS;
    PlayerCarSelection MySlot
    {
        get
        {
            return carS.transform.GetChild(Id).GetComponent<PlayerCarSelection>();
        }
    }
    public static OverriddenNetworkLobbyPlayer LocalVersion = null;
    [System.NonSerialized]private static GameObject _parent = null;
    public Car SelectedCar;
    public bool isBot = false;
    public int Id
    {
        get
        {
            for (int i = 0; i < carS.vars.maxPlayers; i++)
            {
                if (Parent.transform.childCount < i) return -1;
                if (Parent.transform.GetChild(i) == this.gameObject.transform)
                    return i;
            }
            return -1;
        }
    }
    public static GameObject Parent
    {
        get
        {
            if (_parent == null) _parent = Instantiate(Resources.Load("LobbyPlayerHolder") as GameObject);
            return _parent;
        }
    }
    void Awake()
    {
        DontDestroyOnLoad(Parent);
        DontDestroyOnLoad(this.gameObject);
    }
    void Start ()
    {
        carS = FindObjectOfType<CarSelection>();
        transform.parent = Parent.transform;
        for (int i = 0; i < 4; i++) carS.transform.GetChild(i).GetComponent<PlayerCarSelection>().ImHost();
        MySlot.SetActive(true);
        SelectedCar = MySlot.Cars[0];
        if (!hasAuthority) return;
        StartCoroutine(WaitForConnection());
       
    }
    public void AddBot()
    {
        Debug.Log("HEllo!");
        OverriddenNetworkLobbyManager netman = FindObjectOfType<OverriddenNetworkLobbyManager>();
        GameObject bot = Instantiate(netman.lobbyPlayerPrefab.gameObject);
        bot.GetComponent<OverriddenNetworkLobbyPlayer>().isBot = true;
        NetworkServer.Spawn(bot);
    }
    [Command]
    public void CmdUpdateUi(int car, bool Ready)
    {
        RpcUpdateUi(car, Ready);
    }
    [ClientRpc]
    public void RpcUpdateUi(int car, bool Ready)
    {
        SelectedCar = MySlot.Cars[car];
        if (hasAuthority)
        {
            if (Ready) SendReadyToBeginMessage(); else SendNotReadyToBeginMessage();
        }
        if (hasAuthority) return;
        MySlot.SetCarInfo(car);
        MySlot.PlayerReady(Ready);
    }
    [ClientRpc]
    public void RpcIsBot(bool isBot)
    {
        this.isBot = isBot;
    }
    [Command]
    public void CmdRequestUpdate()
    {
        RpcRequestUpdate();
    }
    [ClientRpc]
    public void RpcRequestUpdate()
    {
        if (LocalVersion.isLocalPlayer)
        {
            LocalVersion.CmdUpdateUi(LocalVersion.MySlot.currentChosenCar, LocalVersion.MySlot.Ready);
            LocalVersion.CmdSetName(MainMenu.nickname);
        }
            
    }
    [Command]
    public void CmdSetName(string name)
    {
        RpcSetName(name);
    }
    [ClientRpc]
    public void RpcSetName(string name)
    {
        MySlot.SetPlayerName(name);
}
    public void callCmd(int car, bool Ready)
    {
        CmdUpdateUi(car, Ready);
    }
    public IEnumerator WaitForConnection()
    {
        while (!NetworkClient.active)
            yield return null;

        MySlot.SetActivePlayer(true);
        LocalVersion = this;
        MySlot.Change += callCmd;
        for (int i = 0; i < 4; i++) carS.transform.GetChild(i).GetComponent<PlayerCarSelection>().AddBot += AddBot;
        CmdRequestUpdate();
        if (isBot && isServer) RpcIsBot(true);
    }
    public static OverriddenNetworkLobbyPlayer GetLobbyPlayer(int id)
    {
        OverriddenNetworkLobbyPlayer[] players = FindObjectsOfType<OverriddenNetworkLobbyPlayer>();
        foreach (OverriddenNetworkLobbyPlayer player in players)
            if (player.Id == id) return player;
        return null;
    }
}
