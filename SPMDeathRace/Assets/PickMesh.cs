﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickMesh : MonoBehaviour {

    public GameObject Meshes;
    public BoxCollider HurtBox;
    public BoxCollider ProtectedBox;
    void Start ()
    {
        PlayerController PC = GetComponentInParent<PlayerController>();
        Meshes.transform.GetChild(PC.Id).gameObject.SetActive(true);
        PC.HurtBox.size = HurtBox.size;
        PC.HurtBox.center = HurtBox.center;
        PC.ProtectedBox.size = ProtectedBox.size;
        PC.ProtectedBox.center = ProtectedBox.center;
        ProtectedBox.enabled = false;
        HurtBox.enabled = false;
    }
}
