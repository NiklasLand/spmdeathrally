﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffectReturnToPool : MonoBehaviour {
	void Update () {
        if (!GetComponent<ParticleSystem>().isPlaying)
            ObjectPool.Destroy(this.gameObject);
	}
}
