﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class PauseMenuSelect : MonoBehaviour
{
    public EventSystem ES;
    public GameObject FirstSelected;
    private GameObject _storeSelected;

    private void Awake()
    {
        ES = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        _storeSelected = FirstSelected;
    }

    private void Update()
    {
        if (ES.currentSelectedGameObject != _storeSelected)
        {
            if (ES.currentSelectedGameObject == null)
            {
                ES.SetSelectedGameObject(_storeSelected);
            }
            else
            {
                ChangeSelectColor();
                _storeSelected = ES.currentSelectedGameObject;
                ChangeSelectColor();

            }
        }
        ES.SetSelectedGameObject(_storeSelected);
    }

    private void ChangeSelectColor()
    {
        if (_storeSelected.GetComponentInChildren<TextMeshProUGUI>())
        _storeSelected.GetComponentInChildren<TextMeshProUGUI>().color = _storeSelected.GetComponentInChildren<TextMeshProUGUI>().color == Color.white ? Color.black : Color.white;
    }

    private void OnEnable()
    {
        ES.SetSelectedGameObject(null);
        _storeSelected = FirstSelected;
        ChangeSelectColor();
    }

    private void OnDisable()
    {
        ChangeSelectColor();
        _storeSelected = null;
    }


}
