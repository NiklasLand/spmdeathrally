﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseController : MonoBehaviour {

    private GameObject _pausePanel;
    private GameObject _confirmPanel;

    private void Awake()
    {
        _pausePanel = transform.GetChild(0).gameObject;
        _confirmPanel = transform.GetChild(1).gameObject;
    }

    private void Update()
    {
        //LÄGG IN INPUT START FRÅN KONSOLKONTROLL
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_confirmPanel.activeSelf)
            {
                DisableConfirmPanel();
            } else if (_pausePanel.activeSelf)
            {
                DisablePausePanel();
            } else
            {
                EnablePausePanel();
            }
        }
    }

    public void EnablePausePanel()
    {
        _pausePanel.SetActive(true);
    }

    public void DisablePausePanel()
    {
        _pausePanel.SetActive(false);
    }

    public void EnableConfirmPanel()
    {
        _confirmPanel.SetActive(true);
    }

    public void DisableConfirmPanel()
    {
        _confirmPanel.SetActive(false);
    }

    public void QuitLevel()
    {
        Debug.Log("WE OUTTA HERE");
    }

}
