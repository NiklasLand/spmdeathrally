﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {

    public AudioMixer mixer;
    private float currentVolume;
    private Slider slider;

    //Exposa "MasterVolume" och dra in mixern

    private void Awake()
    {
        slider = GetComponent<Slider>();
        mixer.SetFloat("MasterVolume", GetMasterLevel());
    }

    private void Update()
    {
        mixer.GetFloat("MasterVolume", out currentVolume);
        slider.value = currentVolume;
    }

    public void SetMasterVolume()
    {
        mixer.SetFloat("MasterVolume", slider.value);
    }

    private float GetMasterLevel()
    {
        float value;
        bool result = mixer.GetFloat("MasterVolume", out value);
        if (result)
        {
            return value;
        } else
        {
            return 0f;
        }
    }




}
