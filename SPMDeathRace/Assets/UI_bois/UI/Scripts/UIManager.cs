﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    [Header("Debug")]
    public int TEMPMaxLap = 4;
    //public TEMPCarStat[] cars = new TEMPCarStat[4];
    [Header("References")]
    public Image playerHealthFill;
    public Image playerShadowHealthFill;
    public TextMeshProUGUI playerHealthValue;
    public TextMeshProUGUI playerAmmoValue;
    public GameObject playerPlacement;
    public Image playerBoostImage;
    public GameObject playerLap;
    public GameObject playerAbility;
    public TextMeshProUGUI playerMoney;
    public GameObject raceTimer;
    [Header("UI Colors")]
    public Color32 green;
    public Color32 yellow;
    public Color32 red;
    [Header("Special Ability Icons")]
    public Sprite transAmIcon;
    public Sprite vWBeetleIcon;
    public Sprite fordBroncoIcon;
    public Sprite mustangHardtopIcon;

    private PlayerController pc;

    private bool onCooldown;
    private Coroutine playerHealthCoroutine = null;
    private Coroutine playerMoneyCoroutine = null;

    private void Awake()
    {
        //Please, titta inte på vad vi har gjort.
        #region :^)
        playerHealthFill = GameObject.Find("PlayerHealth").transform.GetChild(1).GetComponentInChildren<Image>();
        playerShadowHealthFill = GameObject.Find("PlayerHealth").transform.GetChild(0).GetComponentInChildren<Image>();
        playerHealthValue = GameObject.Find("PlayerHealth").GetComponentInChildren<TextMeshProUGUI>();
        playerAmmoValue = GameObject.Find("PlayerAmmo").GetComponentInChildren<TextMeshProUGUI>();
        playerPlacement = GameObject.Find("PlayerPlacement");
        playerBoostImage = GameObject.Find("PlayerBoost").GetComponentInChildren<Image>();
        playerLap = GameObject.Find("PlayerLap");
        playerAbility = GameObject.Find("PlayerSpecialAbility");
        playerMoney = GameObject.Find("PlayerMoney").GetComponentInChildren<TextMeshProUGUI>();
        raceTimer = GameObject.Find("RaceTimer");
        raceTimer.SetActive(false);
        #endregion

        pc = PlayerController.LocalVersion;
    }

    private void Start()
    {
        UpdateHealthBarColor(playerHealthFill, playerHealthFill.fillAmount);
        SetPlayerCurrentLap(1);
        SetMaxLaps(TEMPMaxLap);
    }

    private void Update()
    {
        //UpdateHealthBarColor(playerHealthFill);
        //UpdatePlayerAmmo();
        //UpdatePlayerPlacement();
        //UpdatePlayerBoost();
        //UpdatePlayerCurrentLap();
        //Debug.Log(PlayerController.LocalVersion.Body.velocity.magnitude);
        DebugInput();
    }

    //Spelarens hälsa
    public void SetPlayerHealthBar(int currentHealth, int maxHealth)
    {
        if (playerHealthCoroutine != null) StopCoroutine(playerHealthCoroutine);
        playerHealthCoroutine = StartCoroutine(UpdatePlayerTargetHealth(currentHealth, maxHealth));
        playerHealthValue.text = currentHealth.ToString();
        //UpdateHealthBarColor(playerHealthFill, (float)currentHealth / maxHealth);
    }

    //Motståndarens healthbar ändrar värde och färg med referens till bilobjektet.
    public void SetOpponentHealthBar(GameObject car)
    {
        //Sätt cs = motståndarbilens komponent till stats
        TEMPCarStat cs = car.GetComponent<TEMPCarStat>();
        car.GetComponentInChildren<Image>().fillAmount = ((float)cs.currentHealth / cs.maxHealth);
        UpdateHealthBarColor(car.GetComponentInChildren<Image>(), car.GetComponentInChildren<Image>().fillAmount);
    }

    //Motståndarens healthbar ändrar värde och färg med referens till healthbarbilden och health.
    public void SetOpponentHealthBar(Image healthBarImage, int currentHealth, int maxHealth)
    {
        healthBarImage.fillAmount = ((float)currentHealth / maxHealth);
        UpdateHealthBarColor(healthBarImage, healthBarImage.fillAmount);
    }

    //Spelarens ammo
    public void SetPlayerAmmo(int ammo)
    {
        playerAmmoValue.text = ammo.ToString();
    }

    //Spelarenes placering och ändrar färg
    public void SetPlayerPlacement(int placement)
    {
        playerPlacement.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = placement.ToString();
        switch (placement)
        {
            case 1:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "st";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    tmp.color = new Color32(212, 175, 55, 255);
                }
                break;
            case 2:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "nd";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    tmp.color = new Color32(196, 199, 206, 255);
                }
                break;
            case 3:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "rd";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    tmp.color = new Color32(167, 91, 16, 255);
                }
                break;
            case 4:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "th";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    tmp.color = new Color32(255, 255, 255, 255);
                }
                break;
        }
    }

    //Spelarens boost
    public void SetPlayerBoost(int currentBoost, int maxBoost)
    {
        playerBoostImage.fillAmount = ((float)currentBoost / maxBoost);
    }

    //Spelarens nuvarande varv
    public void SetPlayerCurrentLap(int lap)
    {
        playerLap.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = lap.ToString();
    }

    //Banans maxlopp
    public void SetMaxLaps(int lap)
    {
        playerLap.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = lap.ToString();
    }

    //Startar cooldown animation för spelarens special ability
    public void StartAbilityCooldown(int cooldownTimer)
    {
        //if (_abilityEnumerator != null) StopCoroutine(_abilityEnumerator);
        if (!onCooldown)
        {
            StartCoroutine(UpdateAbilityCooldownText(cooldownTimer));
            StartCoroutine(UpdateAbilityCooldownIcon(cooldownTimer));
        }
    }

    //Spelarens pengar, texten ökas gradvis
    public void SetPlayerMoney(int loadsAMoney)
    {
        if (playerMoneyCoroutine != null) StopCoroutine(playerMoneyCoroutine);
        playerMoneyCoroutine = StartCoroutine(UpdatePlayerMoney(loadsAMoney));
    }

    //Startar race countdown
    public void StartRaceTimer()
    {
        raceTimer.gameObject.SetActive(false);
        raceTimer.gameObject.SetActive(true);
    }

    //Byter ikon och färg för spelarens ability baserat 
    public void SetPlayerSpecialAbilityIcon()
    {
        if (pc.Car is TransAm)
        {
            playerAbility.transform.GetChild(0).GetComponent<Image>().color = Color.blue;
            playerAbility.transform.GetChild(1).GetComponent<Image>().sprite = transAmIcon;
        } else if (pc.Car is VWBeetle)
        {
            playerAbility.transform.GetChild(0).GetComponent<Image>().color = Color.cyan;
            playerAbility.transform.GetChild(1).GetComponent<Image>().sprite = vWBeetleIcon;
        } else if (pc.Car is FordBroncoPickup)
        {
            playerAbility.transform.GetChild(0).GetComponent<Image>().color = Color.green;
            playerAbility.transform.GetChild(1).GetComponent<Image>().sprite = fordBroncoIcon;
        } else if (pc.Car is MustangHardtop)
        {
            playerAbility.transform.GetChild(0).GetComponent<Image>().color = Color.red;
            playerAbility.transform.GetChild(1).GetComponent<Image>().sprite = mustangHardtopIcon;
        }
    }

    //Ändrar healthbars färg beroende på värde
    private void UpdateHealthBarColor(Image i, float factor)
    {

        i.color = factor > 0.5f ? Color.Lerp(yellow, green, (factor - 0.5f) * 2) : Color.Lerp(red, yellow, ((factor - 0.2f) * 10 / 3));

        ////DET HÄR FUNKAR OCKSÅ :^)
        //float v = i.fillAmount;
        //if (v >= 0.5)
        //{
        //    i.color = new Color((1 - v) * 2, 1, 0.5f - (1 - v));
        //}
        //else
        //{
        //    i.color = new Color(1, (v * 2) - (0.2f * (1 - v)), 0);
        //}
    }

    //Räknar ner cooldowntext
    private IEnumerator UpdateAbilityCooldownText(int cooldownTimer)
    {
        onCooldown = true;
        playerAbility.GetComponentInChildren<TextMeshProUGUI>().enabled = true;
        for (int i = 0; i < cooldownTimer; i++)
        {

            playerAbility.GetComponentInChildren<TextMeshProUGUI>().text = (cooldownTimer - i).ToString();
            yield return new WaitForSeconds(1);
        }
        playerAbility.GetComponentInChildren<TextMeshProUGUI>().enabled = false;
        onCooldown = false;
        yield return 0;
    }

    //Fyller ut cooldownikonen
    private IEnumerator UpdateAbilityCooldownIcon(int cooldownTimer)
    {
        Image bg = playerAbility.GetComponentInChildren<Image>();
        bg.fillAmount = 0;
        while (bg.fillAmount != 1)
        {
            bg.fillAmount += Time.deltaTime / cooldownTimer;
            yield return 0;
        }
        playerAbility.GetComponent<Animator>().SetTrigger("FinishCD");
        yield return 0;
    }

    //Förflyttar spelarens hälsa med en extra bar i delay
    private IEnumerator UpdatePlayerTargetHealth(int targetHealth, int maxHealth)
    {

        while ((playerHealthFill.fillAmount - (float)targetHealth / maxHealth) > 0.01f)
        {
            playerHealthFill.fillAmount = Mathf.Lerp(playerHealthFill.fillAmount, ((float)targetHealth / maxHealth), 10 * Time.deltaTime);
            UpdateHealthBarColor(playerHealthFill, playerHealthFill.fillAmount);

            yield return 0;
        }
        playerHealthFill.fillAmount = (float)targetHealth / maxHealth;
        yield return new WaitForSeconds(0.5f);
        while ((playerShadowHealthFill.fillAmount - (float)targetHealth / maxHealth) > 0.01f)
        {
            playerShadowHealthFill.fillAmount = Mathf.Lerp(playerShadowHealthFill.fillAmount, ((float)targetHealth / maxHealth), 5 * Time.deltaTime);
            yield return 0;
        }
        playerShadowHealthFill.fillAmount = (float)targetHealth / maxHealth;
        yield return 0;
    }

    //Räknar upp spelarens pengar
    private IEnumerator UpdatePlayerMoney(int money)
    {
        string s = playerMoney.text;
        s = s.Replace(" ", string.Empty);
        int i = int.Parse(s);
        while (money - i > 1)
        {
            s = s.Replace(" ", string.Empty);
            i = int.Parse(s);
            i += 6;
            s = i.ToString();
            if (s.Length > 3)
            {
                s = s.Insert(s.Length - 3, " ");
            }
            playerMoney.text = s;
            yield return 0;
        }
        s = money.ToString();
        if (s.Length > 3)
        {
            s = s.Insert(s.Length - 3, " ");
        }
        playerMoney.text = s;
        yield return 0;
    }

    //Update version av att ändra spelarens värde på hälsa.
    private void UpdatePlayerHealth()
    {
        playerHealthFill.fillAmount = Mathf.Lerp(playerHealthFill.fillAmount, ((float)pc.Car.CurrenHealth / pc.Car.MaxHealth), 0.2f);
        playerHealthValue.text = pc.Car.CurrenHealth.ToString();
    }

    //Update version av att ändra spelarens värde på ammo.
    private void UpdatePlayerAmmo()
    {
        if (pc.Car is AmmoBasedCar)
        {
            AmmoBasedCar c = (AmmoBasedCar)pc.Car;
            playerAmmoValue.text = c.CurrentAmmo.ToString();
        } else if (pc.Car is TransAm)
        {
            TransAm c = (TransAm)pc.Car;
            playerAmmoValue.text = c.CurrentEnergy.ToString();
        }
    }

    //Update version av att ändra spelarens värde på placering.
    private void UpdatePlayerPlacement()
    {
        //Ändra placement
        int placement = 1;
        playerPlacement.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = placement.ToString();
        switch (placement)
        {
            case 1:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "st";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    //Guld
                    tmp.color = new Color32(212, 175, 55, 255);
                }
                break;
            case 2:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "nd";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    //Silver
                    tmp.color = new Color32(196, 199, 206, 255);
                }
                break;
            case 3:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "rd";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    //Brons
                    tmp.color = new Color32(167, 91, 16, 255);
                }
                break;
            case 4:
                playerPlacement.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "th";
                foreach (TextMeshProUGUI tmp in playerPlacement.GetComponentsInChildren<TextMeshProUGUI>())
                {
                    //Jag undrar :^)
                    tmp.color = new Color32(255, 255, 255, 255);
                }
                break;
        }
    }

    //Update version av att ändra spelarens värde på boost.
    private void UpdatePlayerBoost()
    {
        //Ändra om MaxBoost kan variera
        int tempMaxBoostValue = 100;
        playerBoostImage.fillAmount = Mathf.Lerp(playerBoostImage.fillAmount, (float)pc.Car.Turbo / tempMaxBoostValue, 0.2f);
    }

    //ANVÄNDS INTE. Update version av att ändra spelarens värde på varv.
    private void UpdatePlayerCurrentLap()
    {
        //Hitta CurrentLap;
        playerLap.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = 1.ToString();
    }

    //Debug
    private void DebugInput()
    {
        if (Input.GetKeyDown("g"))
        {
            Debug.Log("DEBUG (Knapptryck G): Testar UI värden");
            StartRaceTimer();
            StartAbilityCooldown(5);
            SetPlayerCurrentLap(2);
            SetPlayerAmmo(421);
            SetPlayerPlacement(3);
            SetPlayerBoost(50, 100);
            SetMaxLaps(5);
        }
    }
}
