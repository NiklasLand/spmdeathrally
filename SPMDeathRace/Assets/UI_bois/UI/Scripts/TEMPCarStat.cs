﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEMPCarStat : MonoBehaviour {

    public string nickname;
    public int currentHealth;
    public int maxHealth;
    public int currentAmmo;
    public int placement;
    public int currentBoost;
    public int maxBoost;
    public int currentLap;
    public int money;

    public UIManager UI;

    public char input;

    public void Update()
    {
        if (Input.GetKeyDown(input.ToString()))
        {
            currentHealth -= 10;
            UI.SetOpponentHealthBar(gameObject);
        }
        else if (Input.GetKeyDown("y"))
        {
            currentHealth -= 10;
            UI.SetPlayerHealthBar(currentHealth, maxHealth);
        }
        else if (Input.GetKeyDown("m"))
        {
            money += 100;
            UI.SetPlayerMoney(money);
        }
        else if (Input.GetKeyDown("c"))
        {
            GetComponentInChildren<TrailRenderer>().time = 0;
        }
    }
}
