﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeRaceTimer : MonoBehaviour {

    private TextMeshProUGUI t;
    private int counter = 4;

    private void Awake()
    {
        t = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void CountdownText()
    {
        counter--;
        if (counter != 0)
        {
            t.text = counter.ToString();
        } else
        {
            t.text = "GO";
            counter = 4;
        }
    }
}
