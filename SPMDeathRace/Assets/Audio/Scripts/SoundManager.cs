﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public static SoundManager instance;

	public GameObject audioObjectPrefab;
	[Header("Pick Ups")]
	public AudioClip[] pickUpNitro;
	public AudioClip[] pickUpCash;
	public AudioClip[] pickUpRepair;
	public AudioClip[] pickUpAmmo;
	public AudioClip[] pickUpDrugs;
	[Header("Menu Sounds")]
	public AudioClip menuSelect;
	public AudioClip menuRepair;
	public AudioClip menuUpgrade;
	public AudioClip menuValidate;
	[Header("Weapon Sounds")]
	public AudioClip[] weaponLaser;
	public AudioClip[] weaponMachinegun;
	public AudioClip[] weaponShotgun;
	public AudioClip[] weaponCannon;
	public AudioClip[] impactCannonball;
	public AudioClip[] impactLaser;
	public AudioClip[] impactMachinegun;
	public AudioClip[] impactSpikes;
	public AudioClip energyShield;
	[Header("Car Sounds")]
	public AudioClip dropMine;
	public AudioClip[] skidMarks;
	[Header("SFX Sounds")]
	public AudioClip pedestrian;
	public AudioClip[] explosions;
	public AudioClip[] fireSounds;
	[Header("Voice Sounds")]
	public AudioClip[] startVoice;
	public AudioClip[] winnerVoice;
	public AudioClip[] loserVoice;
	public AudioClip[] raceOverVoice;
	public AudioClip finalLapVoice;



	public enum OneShots {Nitro, Cash, Repair, Ammo, Drugs, MenuSelect, MenuRepair, MenuUpgrade, MenuValidate, Pedestrian, 
		WeaponLaser, WeaponMachinegun, WeaponShotgun, WeaponCannon, DropMine, SkidMark, Explosion, ImpactCannonball, ImpactLaser, ImpactMachinegun, ImpactSpikes,
		EnergyShield, StartVoice, WinnerVoice, LoseVoice, RaceOverVoice, FinalLapVoice
	}
	[HideInInspector]
	public OneShots oneShot;

	private void Awake(){
		instance = this;
	}

	private AudioClip GetAudioClip(OneShots os){
		AudioClip clip = pickUpRepair [0];

		switch (os) {
		case OneShots.Nitro:
			clip = GetRandomClip (pickUpNitro);
			break;
		case OneShots.Cash:
			clip = GetRandomClip (pickUpCash);
			break;
		case OneShots.Repair:
			clip = GetRandomClip (pickUpRepair);
			break;
		case OneShots.Ammo:
			clip = GetRandomClip (pickUpAmmo);
			break;
		case OneShots.Drugs:
			clip = GetRandomClip (pickUpDrugs);
			break;
		case OneShots.MenuSelect:
			clip = menuSelect;
			break;
		case OneShots.MenuRepair:
			clip = menuRepair;
			break;
		case OneShots.MenuUpgrade:
			clip = menuUpgrade;
			break;
		case OneShots.MenuValidate:
			clip = menuValidate;
			break;
		case OneShots.Pedestrian:
			clip = pedestrian;
			break;
		case OneShots.WeaponLaser:
			clip = GetRandomClip (weaponLaser);
			break;
		case OneShots.WeaponMachinegun:
			clip = GetRandomClip (weaponMachinegun);
			break;
		case OneShots.WeaponShotgun:
			clip = GetRandomClip (weaponShotgun);
			break;
		case OneShots.WeaponCannon:
			clip = GetRandomClip (weaponCannon);
			break;
		case OneShots.DropMine:
			clip = dropMine;
			break;
		case OneShots.SkidMark:
			clip = GetRandomClip(skidMarks);
			break;
		case OneShots.Explosion:
			clip = GetRandomClip(explosions);
			break;
		case OneShots.ImpactCannonball:
			clip = GetRandomClip(impactCannonball);
			break;
		case OneShots.ImpactLaser:
			clip = GetRandomClip(impactLaser);
			break;
		case OneShots.ImpactMachinegun:
			clip = GetRandomClip(impactMachinegun);
			break;
		case OneShots.ImpactSpikes:
			clip = GetRandomClip(impactSpikes);
			break;
		case OneShots.EnergyShield:
			clip = energyShield;
			break;
		case OneShots.StartVoice:
			clip = GetRandomClip(startVoice);
			break;
		case OneShots.WinnerVoice:
			clip = GetRandomClip(winnerVoice);
			break;
		case OneShots.LoseVoice:
			clip = GetRandomClip(loserVoice);
			break;
		case OneShots.RaceOverVoice:
			clip = GetRandomClip(raceOverVoice);
			break;
		case OneShots.FinalLapVoice:
			clip = finalLapVoice;
			break;
		default:
			clip = pickUpRepair [0];
			break;
		}
		return clip;
	}




	//INSTANTIATEA IN I POOLEN!!

	public void PlayOneShot(OneShots sound){
		//INSTANTIATEA IN I POOLEN!!
		GameObject n = Instantiate (audioObjectPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
		n.GetComponent<AudioObject>().source.PlayOneShot(GetAudioClip(sound));
		n.GetComponent<AudioObject> ().SetDigetic (false);
	}
	public void PlayOneShot(OneShots sound, float volume){
		//INSTANTIATEA IN I POOLEN!!
		GameObject n = Instantiate (audioObjectPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
		n.GetComponent<AudioObject> ().SetVolume (volume);
		n.GetComponent<AudioObject>().source.PlayOneShot(GetAudioClip(sound));
		n.GetComponent<AudioObject> ().SetDigetic (false);
	}
	public void PlayOneShot(OneShots sound, Vector3 position){
		//INSTANTIATEA IN I POOLEN!!
		GameObject n = Instantiate (audioObjectPrefab, position, Quaternion.Euler(Vector3.zero));
		n.GetComponent<AudioObject>().source.PlayOneShot(GetAudioClip(sound));
		n.GetComponent<AudioObject> ().SetDigetic (true);
	}

	public void PlayFireSound(Vector3 position, float volume){
		//INSTANTIATEA IN I POOLEN!!
		GameObject n = Instantiate (audioObjectPrefab, position, Quaternion.Euler(Vector3.zero));
		n.GetComponent<AudioObject> ().SetVolume (volume);
		n.GetComponent<AudioObject> ().SetDigetic (true);
		n.GetComponent<AudioObject> ().SetLoop (true);
		n.GetComponent<AudioObject> ().PlaySound (GetRandomClip (fireSounds));
	}

	private AudioClip GetRandomClip(AudioClip[] clips){
		return clips [Random.Range (0, clips.Length)];
	}

}
