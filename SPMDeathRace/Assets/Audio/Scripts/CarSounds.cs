﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSounds : MonoBehaviour {
	[Header("Idle")]
	public AudioClip idleSound;
	[Range(0, 1)]
	public float idleVolume = 1;
	public Vector2 idlePitch = new Vector2 (1f, 1.2f);
	[Header("Accelerate")]
	public AudioClip accelerateSound;
	[Range(0, 1)]
	public float accelerateVolume;
	public Vector2 acceleratePitch = new Vector2 (0.9f, 1f);
	[Header("Misc")]
	public float maxVelocity = 2;
	public float velocity = 0f;
	private AudioSource source;
	private bool accelerating = false;
    public PlayerController controller;


	public enum States {EnterIdle, Idle, EnterAccelerate, Accelerate}
	public States state = States.Idle;

	public void Start(){
		source = GetComponent<AudioSource> ();
		source.loop = true;
		state = States.EnterIdle;
        controller = transform.parent.GetComponent<PlayerController>();
	}

	public void Update(){
        //      if (Input.GetKey(KeyCode.Space))
        //      {
        //          accelerating = true;
        //      }
        //      else
        //          accelerating = false;

        //      if (accelerating) {
        //	if (velocity < 2f) {
        //		velocity += Time.deltaTime;
        //	} else {
        //		velocity = 2f;
        //	}
        //} else {
        //	if (velocity > 0) {
        //		velocity -= Time.deltaTime;
        //	} else {
        //		velocity = 0;
        //	}
        //}

        if (Input.GetAxisRaw("Vertical") != 0)
        {
            accelerating = true;
        }
        else
        {
            accelerating = false;
        }

        maxVelocity = 20f;
        if (controller != null)
        velocity = Mathf.Clamp(controller.Body.velocity.magnitude, 0, 20);


		switch (state) {
		case States.EnterIdle:
			source.Stop ();
			source.clip = idleSound;
			source.Play ();
			state = States.Idle;
			break;

		case States.Idle:
			source.pitch = Mathf.Lerp (idlePitch.x, idlePitch.y, velocity / maxVelocity);
			source.volume = idleVolume;
			if (accelerating) {
				state = States.EnterAccelerate;
			}
			break;

		case States.EnterAccelerate:
			source.Stop ();
			source.clip = accelerateSound;
			source.Play ();
			source.pitch = 1f;
			state = States.Accelerate;
			break;

		case States.Accelerate:
			source.pitch = Mathf.Lerp (acceleratePitch.x, acceleratePitch.y, velocity / maxVelocity);
			source.volume = accelerateVolume;
			if (!accelerating) {
				state = States.EnterIdle;
			}
			break;
		}
	}
}
