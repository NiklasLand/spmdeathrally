﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientRandomSounds : MonoBehaviour
{

    public float minDelay, maxDelay;
    public AudioClip[] clips;
    private AudioSource[] sources;
    private int clipIndex = 1;

  
    void Start() {
        sources = GetComponents<AudioSource>();
    }

    void Update() {
        PlayAmbientSound();
    }

    private void PlayAmbientSound() {
        if (!sources[1].isPlaying)
        {
            clipIndex = Random.Range(1, clips.Length);
            AudioClip rndClip = clips[clipIndex];
            sources[1].clip = rndClip;
            float d = Random.Range(minDelay, maxDelay);
            sources[1].PlayDelayed(d);
            clips[clipIndex] = clips[0];
            clips[0] = rndClip;
        }
      
    }
}