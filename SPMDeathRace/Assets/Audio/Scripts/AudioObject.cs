﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour {

	[HideInInspector]
	public AudioClip clip;
	[HideInInspector]
	public AudioSource source;

	private void Awake(){
		source = GetComponent<AudioSource> ();
//		source.PlayOneShot (clip);
	}

	private void Update(){
		if (!source.isPlaying) {
			//ObjectPool.Destroy(gameObject);
			Destroy(gameObject);
		}
	}

	public void SetDigetic(bool isDigetic){
		if (isDigetic)
			source.spatialBlend = 1;
		else
			source.spatialBlend = 0;
	}

	public void SetLoop(bool willLoop){
		if (willLoop)
			source.loop = true;
		else
			source.loop = false;
	}

	public void SetVolume(float volume){
		source.volume = Mathf.Clamp01 (volume);
	}

	public void PlaySound(AudioClip clip){
		source.clip = clip;
		source.Play ();
	}

}
