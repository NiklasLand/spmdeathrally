﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CannonBallLogic : NetworkBehaviour {
    private Vector3 _dir, _velocity;
    private float _speed, _damage, _range, _distanceTraveld = 0f, _knockbackFoce;
    private Car _owner;
    public GameObject ExploationEffect;
    public float ExploationRadius;
    private SphereCollider _myCollider;

    public void Initilize(Vector3 dir, float speed, float damage, float range, float knockBackFroce, Car owner)
    {
        _dir = dir; _speed = speed; _damage = damage; _range = range; _owner = owner; _knockbackFoce = knockBackFroce;
         _velocity = _dir * speed;
        _myCollider = GetComponent<SphereCollider>();
    }
	
    private void UpdateMovement()
    {
        Vector3 toMove = _velocity * Time.deltaTime;
        transform.position += toMove;
        _distanceTraveld += toMove.magnitude;
        if (_distanceTraveld > _range) Expload();
    }
    private void Expload()
    {
        ObjectPool.Instantiate(ExploationEffect, transform.position, Quaternion.identity);
        Collider[] hits = Physics.OverlapSphere(transform.position, ExploationRadius,_owner.MyController.CarLayer);
        foreach(Collider c in hits)
        {
            if (c.GetComponent<PlayerController>() == _owner.MyController) continue;
            Vector3 delta = c.GetComponent<PlayerController>().transform.position - this.transform.position;
            _owner.MyController.CmdDamageOtherPlayer((int)_damage, c.GetComponent<PlayerController>().Id, c.GetComponent<PlayerController>().transform.position, delta, _knockbackFoce);
        }
        ObjectPool.Destroy(this.gameObject);
    }
    private void UpdateCollision()
    {
        Collider[] hits = Physics.OverlapSphere(_myCollider.center,_myCollider.radius);
        foreach (Collider c in hits)
        {
            if (c.GetComponent<PlayerController>() == _owner.MyController) continue;
            Expload();
        }
    }
	void Update () {
        UpdateMovement();
        UpdateCollision();
    }
}
