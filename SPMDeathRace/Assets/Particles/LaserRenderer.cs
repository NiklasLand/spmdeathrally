﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRenderer : MonoBehaviour {
    
    private LineRenderer _lr;
    public GameObject[] laserPrefab;


    void Start () {

        _lr = GetComponent<LineRenderer>();
		
	}
	
	void Update () {

        laserPrefab[0].transform.position = _lr.GetPosition(0);
        laserPrefab[1].transform.position = _lr.GetPosition(1);
    }
    
}
