﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "States/DeathState")]
public class DeathState : State {

    public GameObject ExploationEffect;
    private PlayerController _controller;
    public override void Initialize(Controller owner)
    {
        _controller = (PlayerController)owner;
    }
    public override void Enter()
    {
        base.Enter();
        GameObject temp = ObjectPool.Instantiate(ExploationEffect);
        temp.transform.position = _controller.transform.position;
    }
}
