﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public struct Wheel
{
    public string Name;
    public Transform WheelTransform;
    public bool CanTurn;
    public float MaxTurningDegree;
    public bool IsDriving;
    public float FrictionCoefficient;
    public bool IsSkidding;
}
[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class PlayerController : Controller{
    [Header("PlayerStats")]
    public string NickName;
    public int Money = 500;
    public bool CanDrive = false;
    public static PlayerController LocalVersion;
    [Header("Cars")]
    public GameObject[] Cars;
    public Car Car;
    public Rigidbody Body { get; set; }
    public BoxCollider HurtBox;
    public BoxCollider ProtectedBox;
    public Transform Transform { get { return transform; } }
    public Vector2 InputAxis;
    public bool[] Buttons = new bool[3];
    public bool[] ButtonsDown = new bool[3];
    public bool[] ButtonsRelease = new bool[3];
    public LayerMask CarLayer;
    public float TackleForce;
    public float TackleDamage;
    public int Id = -1;

    //Buttens[0] = nitro, Buttens[1] = Fire, Buttens[2] = Special
    public Wheel[] AllWheels;
    [Range(0, 1)]
    public float InputRequiredToMove;
    public int currentLap;
    public int checkpointCounter;
    public bool marker1Passed;
    public bool marker2Passed;
    public GameObject HitEffect;

    private void Start()
    {
        if (isLocalPlayer)
        {
            Car.CurrenHealth = 100;
            Money = 500;
            Debug.Log("local player");
            LocalVersion = this;
            StartCoroutine(WaitForConnection());
        }
        
        if(GetComponent<AIBehaviour>() != null)
        {
            Car = Instantiate(Car);
            Car.Initialize(this);
            GameObject temp = Instantiate(Car.VisualPrefab);
            temp.transform.SetParent(this.transform);
            temp.transform.localPosition = Vector3.zero;
        }
        
        Body = GetComponent<Rigidbody>();
    }
    [ClientRpc]
    public void RpcCanDrive(bool canDrive)
    {
        CanDrive = canDrive;
    }

    [ClientRpc]
    public void RpcGiveMoney(int money)
    {
        Money += money;
    }
    [ClientRpc]
    public void RpcTimeToSetIDs()
    {
        if (isLocalPlayer)
        {
            Debug.Log(OverriddenNetworkLobbyPlayer.LocalVersion.Id);
            CmdSetID(OverriddenNetworkLobbyPlayer.LocalVersion.Id);
        }
        else
        {
            Debug.Log(OverriddenNetworkLobbyPlayer.LocalVersion.Id + " is not localPlayer");
        }
    }
    [Command]
    private void CmdImReady()
    {
        Debug.Log("ClientReady");
        GameManager.instance.readyPlayers++;
    }
    [Command]
    private void CmdSetID(int id)
    {
        RpcSetID(id);
    }
    [ClientRpc]
    private void RpcSetID(int id)
    {
        Id = id;
        Car = OverriddenNetworkLobbyPlayer.GetLobbyPlayer(Id).SelectedCar;
        Car = Instantiate(Car);
        Car.Initialize(this);
        GameObject temp =Instantiate(Car.VisualPrefab);
        temp.transform.SetParent(this.transform);
        temp.transform.localPosition = Vector3.zero;
        temp.transform.localRotation = Quaternion.identity;
    }
    [Command]
    public void CmdDamageOtherPlayer(int Damage, int HitPlayerID, Vector3 relativeHitPoint, Vector3 HitDirection, float knockBackForce)
    {
        RpcDamageOtherPlayer(Damage, Id, HitPlayerID, relativeHitPoint, HitDirection, knockBackForce);
    }
    [ClientRpc]
    public void RpcDamageOtherPlayer(int Damage,int AttackId, int HitPlayerID, Vector3 relativeHitPoint, Vector3 HitDirection, float knockBackForce)
    {
        GameObject temp = ObjectPool.Instantiate(HitEffect);
        temp.transform.position = relativeHitPoint;

        PlayerController pc = GameManager.instance.GetPlayer(HitPlayerID);
        if (!pc.hasAuthority) return;
        pc.Car.TakeDamage(Damage);
        pc.Body.AddForceAtPosition(knockBackForce * HitDirection, relativeHitPoint + transform.position);
        if (pc.Car.CurrenHealth <= 0)
        {
            pc.CmdIDead(AttackId);
        }
    }
    [ClientRpc]
    public void RpcMovePlayerToStartingLine(Vector3 position, Quaternion rotation)
    {
            Body.velocity = Vector3.zero;
            Body.angularVelocity = Vector3.zero;
            transform.position = position;
            transform.rotation = rotation;
    }
    [Command]
    public void CmdIDead(int Killer)
    {
        RpcIDead(Killer);
        TransitionToState<DeathState>();
    }
    [ClientRpc]
    public void RpcIDead(int Killer)
    {
        if (isLocalPlayer) return;
        TransitionToState<DeathState>();
    }
    [Command]
    public void CmdMakeCarFire()
    {
        RpcMakeCarFire();
    }
    [ClientRpc]
    public void RpcMakeCarFire()
    {
        if (isLocalPlayer) return;
        Car.Fire();
    }
    public IEnumerator WaitForConnection()
    {
        while (!NetworkClient.active)
            yield return null;
        CmdImReady();
        Debug.Log("ImReady");
    }
    void OnCollisionEnter(Collision collision)
    {

        if (!isLocalPlayer) return;
        PlayerController TackledCar = collision.gameObject.GetComponent<PlayerController>();
        if (TackledCar == null || TackledCar == this) return;
        // Debug-draw all contact points and normals
        Vector3 avragePoint = Vector3.zero;
        int hits = 0;
        foreach (ContactPoint contact in collision.contacts)
        {
            hits++;
            avragePoint += contact.point;
        }
        avragePoint /= hits;
        Vector3 relative = Body.velocity - TackledCar.Body.velocity;
        if (relative.magnitude < 2f) return;
        //CmdDamageOtherPlayer((int)(TackleDamage * collision.relativeVelocity.magnitude), TackledCar.Id,avragePoint - TackledCar.transform.position, Vector3.zero, 0f);
    }
}
