﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour, IPointerEnterHandler
{


    public void OnPointerEnter(PointerEventData data)
    {
        UIAudioManager.audioManager.PlayClip(UIAudioManager.audioManager.hightlightClip);
    }

    public void PlayDefaultPressedAudio()
    {
        UIAudioManager.audioManager.PlayClip(UIAudioManager.audioManager.pressedClip);
    }
    public void PlayUpgradeAudio()
    {
        UIAudioManager.audioManager.PlayClip(UIAudioManager.audioManager.upgradeClip);
    }

    public void PlayRepairAudio()
    {
        UIAudioManager.audioManager.PlayClip(UIAudioManager.audioManager.repairClip);
    }
}
