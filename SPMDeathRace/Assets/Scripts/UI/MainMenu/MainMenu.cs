﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public delegate void Hook();
    public delegate void StringHook(string name);
    public Hook Host;
    public StringHook Join;
    public GameObject StartMenu;
    public GameObject LobbyMenu;
    public Text nameInputField, IpInputField;
    [HideInInspector] public static string nickname;
    [HideInInspector] public string IPAdress;

    private void Start()
    {
        UIAudioManager.audioManager.PlayBackgroundMusic();
    }

    //Called by exitbutton
    public void ExitGame()
    {
        Application.Quit();
    }
    //called by singleplayerbutton
    public void StartSingleplayerGame()
    {
        if (Host != null) Host();
        StartMenu.SetActive(false);
        LobbyMenu.SetActive(true);
    }

    //called by ok button
    public void SetNickname()
    {
        nickname = nameInputField.text;
        Debug.Log(nickname);
    }

    //called by host button
    public void HostGame()
    {
        StartMenu.SetActive(false);
        LobbyMenu.SetActive(true);
        if (Host != null) Host();
    }

    //called by joinbutton
    public void JoinGame()
    {
        StartMenu.SetActive(false);
        LobbyMenu.SetActive(true);
        IPAdress = IpInputField.text;
        if (Join != null) Join(IPAdress);
    }



}
