﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CarSelection : MonoBehaviour {
    //Övergripande global koll på allt

    private void OnDisable()
    {
        UIAudioManager.audioManager.StopPlayingBackgroundMusic();
    }

    [System.Serializable]
    public class VariablesList : System.Object
    {
        public int maxPlayers = 4;
    }

    public VariablesList vars = new VariablesList();

    private int readyPlayersCount = 0;

    public void LeaveGame()
    {
        //Disconnect from network-stuff?
        Debug.Log("Disconnect and leave game n stuff");
    
    }


    public void SetPlayerReady(bool isReady)
    {
        readyPlayersCount = isReady ? ++readyPlayersCount : --readyPlayersCount;
        if(readyPlayersCount == vars.maxPlayers)
            RunGame();
    }

    private void RunGame()
    {
        Debug.LogError("Insert race scene name or index below");
        SceneManager.LoadScene("InsertRaceSceneName");
    }
}
