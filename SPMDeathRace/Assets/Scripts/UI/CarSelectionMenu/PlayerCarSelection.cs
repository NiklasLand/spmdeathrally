﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCarSelection : MonoBehaviour {

    public delegate void Hook(int car, bool Ready);
    public Hook Change;
    public delegate void Hookvoid();
    public Hookvoid AddBot;
    [Header("CarStats ScriptableObjects")]
    public Car[] Cars;
    public GameObject Slot;
    public GameObject BotAddSlot;
    public bool Host = false;
    [System.Serializable]
    public class References : System.Object
    {
        public CarSelection carSelection;
        public Text playerNameText;
        public Text carName;
        public Image carImage, carStatsImage;
        public GameObject switchCarButtons;
        public Button readyButton;
    }
    public int currentChosenCar = 0;
    public bool Ready = false;
    public References refs;

    private void Start()
    {
        //vill NetworkB ha allt sant eller falskt i start??
        refs.switchCarButtons.SetActive(false); //false
        refs.readyButton.interactable = false; //false
        SetCarInfo(currentChosenCar);
    }

    //Denna aktiverar interaktivitet till spelarens meny
    public void ImHost()
    {
        Host = true;
        BotAddSlot.SetActive(true);
    }
    public void SetActivePlayer(bool active)
    {
        refs.switchCarButtons.SetActive(active);
        refs.readyButton.interactable = true;
    }
    public void SetCarInfo(int index)
    {
        refs.carImage.sprite = Cars[index].LobbyUiData.carSprite;
        refs.carStatsImage.sprite = Cars[index].LobbyUiData.statsSprite;
        refs.carName.text = Cars[index].LobbyUiData.carName;
    }
    public void SetActive(bool active)
    {
        Slot.SetActive(active);
        if(Host)
            BotAddSlot.SetActive(!active);
    }
    public void SetPlayerName(string playerName)
    {
        refs.playerNameText.text = playerName;
    }
    public void PlayerReady(bool Ready)
    {
        refs.readyButton.GetComponentInChildren<Text>().text = Ready ? "Ready!" : "Ready?"; //hårdkodad o fint
        if(this.Ready != Ready)refs.carSelection.SetPlayerReady(Ready);
        this.Ready = Ready;
        foreach (Transform child in refs.switchCarButtons.transform)
            child.GetComponent<Button>().interactable = !Ready;
        if (Change != null) Change(currentChosenCar, Ready);
    }
    ////////////////////////// METHODS CALLED BY BURRITOS ///////////////////////////////////////////
    //Called by next/prev car buttons
    public void SwitchCar(bool goRight)
    {
        if (goRight)
        {
            currentChosenCar++;
            if (currentChosenCar == Cars.Length)
                currentChosenCar = 0;
            SetCarInfo(currentChosenCar);
        }
        else
        {
            currentChosenCar--;
            if (currentChosenCar < 0)
                currentChosenCar = Cars.Length - 1;
            SetCarInfo(currentChosenCar);
        }
        if(Change != null)Change(currentChosenCar,Ready);
    }
    public void AddBotButFunc()
    {
        if(AddBot != null) AddBot();
    }
    //Called by ready button
    public void PlayerButtonReady()
    {
        PlayerReady(!Ready);
    }
}
