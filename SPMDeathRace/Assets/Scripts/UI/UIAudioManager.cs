﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioManager : MonoBehaviour {
    
    public static UIAudioManager audioManager;

    private AudioSource[] audioSources;

    public AudioClip hightlightClip, pressedClip, repairClip, upgradeClip;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if(audioManager == null)
        {
            audioManager = this;
        }
        audioSources = GetComponents<AudioSource>();
    }

    public void PlayClip(AudioClip clip)
    {
        audioSources[0].PlayOneShot(clip);
    }

    public void PlayBackgroundMusic()
    {
        audioSources[1].Play();
    }

    public void StopPlayingBackgroundMusic()
    {
        audioSources[1].Stop();
    }




}
