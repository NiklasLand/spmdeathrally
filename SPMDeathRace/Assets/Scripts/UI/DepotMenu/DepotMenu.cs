﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DepotMenu : MonoBehaviour
{
    public PlayerController playerController;
    public float depotTimerTime;
    public Text timerText;

    public UpgradeBar[] upgradeBars;

    public Text playerMoneyText;

    public Image carImage;

    public Slider healthSlider;

    public Button repairButton;

    public Upgrade[] lobbyUpgrades;

    [HideInInspector] public bool timesUp = false;

    //public to talk with networkB?
    private int currentArmorStatus = 0;
    private int currentWeaponStatus = 0;
    private int currentEngineStatus = 0;
    private int currentTiresStatus = 0;
    //*

    private Car playerCar;

    public void MyEnable()
    {

        UIAudioManager.audioManager.PlayBackgroundMusic();
        if (PlayerController.LocalVersion == null)
        {
            Debug.Log("Depot couldnt find localplayer");
            return;
        }
            
       
        playerController = PlayerController.LocalVersion;
        playerCar = playerController.Car;
        Debug.Log(playerController.Money);

        //playerMoneyText.text = playerController.Money + "";
        SetPlayerMoney();
        UpdateCurrentHealth();
        // FindExistingUpgrades();
        carImage.sprite = playerCar.LobbyUiData.carSprite;
       
        //sätt spelar $$$ (och namn?), hitta spelarens bil, kalla på findExistingUpgrades(). sätt bilens imageSprite (finns i car?).
    }

    private void OnDisable()
    {
        UIAudioManager.audioManager.StopPlayingBackgroundMusic();
    }

    private bool CheckAndUpdateMoney(int index, int status)
    {

        int cost = upgradeBars[index].CheckCostOfUpgrade(status);
        if (cost > playerController.Money)
            return false;
        playerController.Money -= cost; //temp, hämta och påverka car.money eller player.money
        playerMoneyText.text = playerController.Money + "";

        return true;
    }

    public void UpdateCurrentHealth()
    {
        healthSlider.value = playerCar.CurrenHealth;
        if (playerCar.CurrenHealth >= playerCar.MaxHealth)
            repairButton.interactable = false;
    }

    public void FindExistingUpgrades()
    {

        List<Upgrade> carUpgrades = playerCar.GetAllUpgrades();

        foreach (Upgrade up in carUpgrades)
        {
            if (up is MaxArmourUpgrade)
                currentArmorStatus++;
            else if (up is DamageUP)
                currentWeaponStatus++;
            else if (up is UpgradeSpeed)
                currentEngineStatus++;
            else if (up is TireUpgrade)
                currentTiresStatus++;
        }

        upgradeBars[0].UpdateUpgradeStatus(currentArmorStatus);
        upgradeBars[1].UpdateUpgradeStatus(currentWeaponStatus);
        upgradeBars[2].UpdateUpgradeStatus(currentEngineStatus);
        upgradeBars[3].UpdateUpgradeStatus(currentTiresStatus);

    }

    public void SetCarSprite(Sprite carSprite)
    {
        carImage.sprite = carSprite;
    }

    public void SetPlayerMoney()
    {
        playerMoneyText.text = playerController.Money + "";
    }

    public void StartTimer()
    {
        StartCoroutine(DepotTimer());

    }

    IEnumerator DepotTimer()
    {
        timerText.text = depotTimerTime + "";
        float time = depotTimerTime;
        while (time > -1)
        {
            yield return new WaitForSeconds(1f);
            timerText.text = --time + "";
        }
        timesUp = true;
    }

    /*
    ----------------------------------------Methods called by buttons -------------------------------------------
    */

    public void UpgradeArmor()
    {

        if (!CheckAndUpdateMoney(0, currentArmorStatus))
            return;

        currentArmorStatus++;
        upgradeBars[0].UpdateUpgradeStatus(currentArmorStatus);
        playerCar.EquipUpg(lobbyUpgrades[0]);
    }

    public void UpgradeWeapon()
    {
        if (!CheckAndUpdateMoney(1, currentWeaponStatus))
            return;
        currentWeaponStatus++;
        upgradeBars[1].UpdateUpgradeStatus(currentWeaponStatus);
        playerCar.EquipUpg(lobbyUpgrades[1]);
    }

    public void UpgradeEngine()
    {
        if (!CheckAndUpdateMoney(2, currentEngineStatus))
            return;
        currentEngineStatus++;
        upgradeBars[2].UpdateUpgradeStatus(currentEngineStatus);
        playerCar.EquipUpg(lobbyUpgrades[2]);
    }

    public void UpgradeTires()
    {
        if (!CheckAndUpdateMoney(3, currentTiresStatus))
            return;
        currentTiresStatus++;
        upgradeBars[3].UpdateUpgradeStatus(currentTiresStatus);
        playerCar.EquipUpg(lobbyUpgrades[3]);
    }

    public void Repair()
    {

        if (playerController.Money < 20)
            return;

        if (playerCar.CurrenHealth >= playerCar.MaxHealth)
            return;

        playerCar.EquipUpg(lobbyUpgrades[4]);
        playerController.Money -= 20;
        playerMoneyText.text = playerController.Money + "";
        if (playerCar.CurrenHealth > playerCar.MaxHealth)
            playerCar.CurrenHealth = playerCar.MaxHealth;
        UpdateCurrentHealth();
    }

    public void Sabotage()
    {
        /*
        if (playerController.Money < 300)
            return;

        playerController.Money -= 300;
        playerMoneyText.text = playerController.Money + "";
        */
        //??? does stuff mayhaps?

    }

}
