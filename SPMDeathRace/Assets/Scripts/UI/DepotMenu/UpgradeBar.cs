﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UpgradeBar : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Color notUpgradedColor, upgradedColor;
    public List<int> upgradeCosts;
    private List<Image> upgradeImages = new List<Image>();

    private Color greenColor = new Color32(143, 230, 134, 255);
    public Button upgradeButton;
    public Text upgradeCostText;
    private int currentUpgrade = 0;

    private void Start()
    {
        for (int i = 1; i < 4; i++)
        {
            Image image = transform.GetChild(i).GetComponent<Image>();
            image.color = notUpgradedColor;
            upgradeImages.Add(image);
        }

        if (!upgradeButton)
            upgradeButton = GetComponentInChildren<Button>();

        if (!upgradeCostText)
            upgradeCostText = upgradeButton.transform.GetChild(1).GetComponent<Text>();

        upgradeCostText.text = upgradeCosts[0] + "";
    }

    public void UpdateUpgradeStatus(int imageToChange)
    {
        for (int i = 0; i < imageToChange; i++)
        {
            upgradeImages[i].color = upgradedColor;
        }
        if (imageToChange == 3)
        {
            upgradeButton.gameObject.SetActive(false);
            upgradeCostText.gameObject.SetActive(false);
            return;
        }
        upgradeCostText.text = upgradeCosts[imageToChange] + "";
        currentUpgrade = imageToChange;
    }

    public int CheckCostOfUpgrade(int index)
    {
        return upgradeCosts[index];
    }

    public void OnPointerEnter(PointerEventData pointerData)
    {
        if (pointerData.position.x > 830f)
        {
            if (currentUpgrade < 3)
                upgradeImages[currentUpgrade].color = greenColor;
        }
    }

    public void OnPointerExit(PointerEventData pointerData)
    {
        if (currentUpgrade < 3)
            upgradeImages[currentUpgrade].color = new Color32(0, 0, 0, 255);
    }

}
