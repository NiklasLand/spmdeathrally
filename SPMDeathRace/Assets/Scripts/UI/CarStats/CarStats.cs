﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "UI/CarStats")]
public class CarStats : ScriptableObject {

    public Sprite carSprite;
    public Sprite statsSprite;
    public string carName;

}
