﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "States/OriginalState")]
public class OriginalState : State
{ 
    public float Acceleration { get { return _controller.Car.Acceleration; } set { _controller.Car.Acceleration = value; } }
    public float MaxSpeed { get { return _controller.Car.Speed; } set { _controller.Car.Speed = value; } }
    public float StartBrakeSpeed;
    public float SkidForce = 2f;
    private PlayerController _controller;
    public override void Initialize(Controller owner)
    {
        _controller = (PlayerController)owner;
    }

    public override void Update()
    {
        if (!_controller.CanDrive) return;
        if (!_controller.hasAuthority)
        {
            for (int i = 0; i < _controller.AllWheels.Length; i++)
            {
                _controller.AllWheels[i].IsSkidding = false;
                Wheel w = _controller.AllWheels[i];
                float NormalForce = Vector3.Dot(_controller.Body.GetPointVelocity(w.WheelTransform.position), w.WheelTransform.right);
                if (Mathf.Abs(NormalForce) > SkidForce)
                    _controller.AllWheels[i].IsSkidding = true;
            }
        }
        if (!_controller.hasAuthority) return;
        if(_controller.Car.UpdateCarDelegate != null) _controller.Car.UpdateCarDelegate();
        if (_controller.Buttons[1]) _controller.Car.Fire();
        if (_controller.Buttons[2]) _controller.Car.Special();
        _controller.Car.TurboBoost(_controller.Buttons[0]);
        for (int i = 0; i < _controller.AllWheels.Length; i++)
        {
            Wheel w = _controller.AllWheels[i];
            _controller.AllWheels[i].IsSkidding = false;
            if (w.CanTurn) w.WheelTransform.localRotation = Quaternion.Euler(0f, w.MaxTurningDegree * _controller.InputAxis.x, 0);
            if(_controller.Body.velocity.magnitude < MaxSpeed && w.IsDriving)
                _controller.Body.AddForceAtPosition(_controller.InputAxis.y * Acceleration * Time.deltaTime * w.WheelTransform.forward, w.WheelTransform.position);

            float NormalForce = Vector3.Dot(_controller.Body.GetPointVelocity(w.WheelTransform.position), w.WheelTransform.right);
            _controller.Body.AddForceAtPosition(NormalForce * w.WheelTransform.right * -1f * w.FrictionCoefficient * Time.deltaTime, w.WheelTransform.position);
            if (Mathf.Abs(NormalForce) > SkidForce)
                _controller.AllWheels[i].IsSkidding = true;
        }
        if (_controller.Body.velocity.magnitude > StartBrakeSpeed && Vector3.Dot(_controller.Body.velocity, _controller.transform.forward) > 0 && _controller.InputAxis.y < _controller.InputRequiredToMove)
            Brake();
    }
    


    public void Brake()
    {
        for (int i = 0;i < _controller.AllWheels.Length;i++)
        {
            _controller.Body.AddForceAtPosition(-_controller.Body.GetPointVelocity(_controller.AllWheels[i].WheelTransform.position) * _controller.AllWheels[i].FrictionCoefficient * Time.deltaTime, _controller.AllWheels[i].WheelTransform.position);
            _controller.AllWheels[i].IsSkidding = true;
        }
    }

    public override void Exit()
    {

    }
}
