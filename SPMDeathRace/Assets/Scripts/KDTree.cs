﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KDTree {

    private TreeNode treeRoot;

    public TrackNode NearestNeighbour(Vector2 position)
    {
        TreeNode node = NearestNeighbour(treeRoot, position, null, 0);
        if (node != null)
            return node.value;

        return null;
    }

    public bool IsEmpty()
    {
        return treeRoot == null;
    }

    private TreeNode NearestNeighbour(TreeNode here, Vector2 point, TreeNode best, int dimension)
    {
        if (here == null)
        {
            if (best == null)
                Debug.Log("tree is empty??");

            return best;
        }

        if (best == null)
            best = here;

        if (Vector2.Distance(new Vector2(here.value.X, here.value.Y), point) < Vector2.Distance(new Vector2(best.value.X, best.value.Y), point))
            best = here;

        float hereValue = dimension == 0 ? here.value.X : here.value.Y;
        float pointValue = dimension == 0 ? point.x : point.y;

        TreeNode childNear, childFar;

        if (pointValue < hereValue)
        {
            childNear = here.left;
            childFar = here.right;
        }
        else
        {
            childNear = here.right;
            childFar = here.left;
        }

        best = NearestNeighbour(childNear, point, best, (dimension + 1) % 2);

        if (DistanceLowerBounds(here, point, dimension) < Vector2.Distance(new Vector2(best.value.X, best.value.Y), point))
            best = NearestNeighbour(childFar, point, best, (dimension + 1) % 2);

        return best;

    }

    private float DistanceLowerBounds(TreeNode first, Vector2 secons, int dimension)
    {
        return Mathf.Abs(dimension == 0 ? first.value.X - secons.x : first.value.Y - secons.y);
    }

    public TrackNode GetNestNode(Vector2 position)
    {
        return NearestNeighbour(position).GetNextNode();
    }

    public TrackNode Insert(TrackNode node)
    {
        treeRoot = Insert(node, treeRoot, 0);
        return treeRoot.value;
    }

    private TreeNode Insert(TrackNode node, TreeNode root, int level)
    {
        if (root == null)
            return new TreeNode(node);

        float nodeValue = level == 0 ? node.X : node.Y;
        float rootValue = level == 0 ? root.value.X : root.value.Y;

        if (nodeValue < rootValue)
            root.left = Insert(node, root.left, (level + 1) % 2);
        else
            root.right = Insert(node, root.right, (level + 1) % 2);

        return root;
    }

    [System.Serializable]
    public class TreeNode
    {
        [SerializeField]
        public TrackNode value;
        public TreeNode left, right;

        public TreeNode(TrackNode node)
        {
            this.value = node;
        }

        public bool IsLeaf()
        {
            return left == null && right == null;
        }
    }
}
