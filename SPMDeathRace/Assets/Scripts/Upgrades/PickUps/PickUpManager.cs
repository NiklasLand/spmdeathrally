﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpManager : MonoBehaviour {

    //Koppla till PickUp prefab (Alexander och Calle jobbade med PickUp(2)!!!!!!!!!!)
    public GameObject PickUp;

    //Lista över SpawnPoints, vi la ut tomma GameObjects överallt som var barn till PickUpManager
    public List<Transform> spawnPoints = new List<Transform>();
    private List<GameObject> activePickUps = new List<GameObject>();

    private void Start()
    {
        StartCoroutine(SpawnPickUp());
    }

    private IEnumerator SpawnPickUp()
    {
        if (activePickUps.Count < spawnPoints.Count)
        {
            bool foundInactive = false;
            int index = 0;
            while (!foundInactive)
            {
                index = Random.Range(0, spawnPoints.Count);
                Transform t = spawnPoints[index];
                foundInactive = true;
                foreach (GameObject go in activePickUps)
                {

                    if (go.transform.parent.transform == t)
                    {
                        foundInactive = false;
                    }
                }
            }
            GameObject p = Object.Instantiate(PickUp, spawnPoints[index]);
            p.GetComponent<PhysicalUpgrade>().Initialize();
            activePickUps.Add(p);
        }
        yield return new WaitForSeconds(5);
        StartCoroutine(SpawnPickUp());
        yield return 0; 
    }

}
