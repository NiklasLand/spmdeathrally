﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/Pickups/Mushroom")]
public class MushroomPickup : PickUp {
    public float CameraWobbleDuration;
    public override void OnEquip()
    {
        CallCameraWobble();
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
        
    }
    public void CallCameraWobble()
    {
        Camera.main.GetComponent<CameraController>().AddCameraWobble(CameraWobbleDuration);
    }
 
}
