﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/Pickups/Refuel")]
public class RefuelTurboPickUp : PickUp {
    public float RefuelAmountProcentage;
    public override void OnEquip()
    {
        int RefuelAmount = Mathf.CeilToInt(100 * RefuelAmountProcentage);

        if (Owner.Turbo + RefuelAmount < 100)
            Owner.Turbo += RefuelAmount;
        else Owner.Turbo = 100;

        if (!SaveItem) Owner.UnequipUpg(this);


        Owner.Turbo += RefuelAmount;
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
        
    }
}
