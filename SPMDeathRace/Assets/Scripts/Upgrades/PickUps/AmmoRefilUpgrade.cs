﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/Pickups/AmmoRefill")]
public class AmmoRefilUpgrade : PickUp {
    public float AmmoAmountProcentage;
    public override void OnEquip()
    {
        if (!(Owner is TransAm))
        {
            AmmoBasedCar tempOwner = (AmmoBasedCar)Owner;
            int AmmoAmount = (int)Mathf.Ceil(tempOwner.MaxAmmo * AmmoAmountProcentage);
            if (tempOwner.CurrentAmmo + AmmoAmount < tempOwner.MaxAmmo)
                tempOwner.CurrentAmmo += AmmoAmount;
            else tempOwner.CurrentAmmo = tempOwner.MaxAmmo;
        }
        else
        {
            TransAm tempOwner = (TransAm)Owner;
            tempOwner.CurrentEnergy = tempOwner.CurrentEnergy + AmmoAmountProcentage > 100 ? tempOwner.CurrentEnergy + AmmoAmountProcentage : 100;
        }
        if (!SaveItem) Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
        
    }
}
