﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/LobbyUpgrade/ArmourUp")]
public class MaxArmourUpgrade : Upgrade {
    public int ArmourUp;
    public override void OnEquip()
    {
        Owner.Armour += ArmourUp;
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
      
    }
}
