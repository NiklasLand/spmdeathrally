﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/LobbyUpgrade/SpeedUp")]
public class UpgradeSpeed : Upgrade {
    public int SpeedUp;
    public override void OnEquip()
    {
        Owner.Speed += SpeedUp;
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
        throw new System.NotImplementedException();
    }
}
