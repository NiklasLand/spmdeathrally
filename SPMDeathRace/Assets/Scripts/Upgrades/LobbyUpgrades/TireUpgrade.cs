﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/LobbyUpgrade/TireUpgrade")]
public class TireUpgrade : Upgrade {
    public float TireFrictionUp;
    public override void OnEquip()
    {
        for(int i = 0; i < Owner.MyController.AllWheels.Length; i++)
        {
            Owner.MyController.AllWheels[i].FrictionCoefficient += TireFrictionUp;
        }
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
    }

}
