﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/LobbyUpgrade/DamageUp")]
public class DamageUP : Upgrade {
    public int DamageAmount;
    public override void OnEquip()
    {
        Owner.Damage += 1;
        Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
        
    }
}
