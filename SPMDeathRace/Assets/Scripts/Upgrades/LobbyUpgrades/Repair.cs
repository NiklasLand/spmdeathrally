﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Upgrades/Pickups/Repair")]
public class Repair : Upgrade {
    public float HealthProcentage;
    public override void OnEquip()
    {
        int HealthAmount = (int)Mathf.Ceil(Owner.MaxHealth * HealthProcentage);

        if (Owner.CurrenHealth + HealthAmount < Owner.MaxHealth)
            Owner.CurrenHealth += HealthAmount;
        else Owner.CurrenHealth = Owner.MaxHealth;
        
        if (!SaveItem) Owner.UnequipUpg(this);
    }

    public override void OnUnEquip()
    {
    }
}
