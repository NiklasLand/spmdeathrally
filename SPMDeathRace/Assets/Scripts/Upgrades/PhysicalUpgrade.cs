﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalUpgrade : MonoBehaviour {
    //public SpriteRenderer VisualRepresentation;
    private PickUp _upgradeType;
    public List<PickUp> UpgradeTypes = new List<PickUp>();
    public LayerMask CarLayer;
    private BoxCollider UpgradeCollider;

    public void Initialize(PickUp ForcePickUp = null)
    {
        UpgradeCollider = GetComponent<BoxCollider>();
        if (ForcePickUp != null)
        {
            _upgradeType = ForcePickUp;
        }
        else
        {
            _upgradeType = SetRandomPickUp();
        }

        SetUpObject();
    }

    private void SetUpObject()
    {
        //VisualRepresentation = _upgradeType.VisualRepresentation;
        if (_upgradeType is AmmoRefilUpgrade)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        } else if (_upgradeType is MushroomPickup)
        {
            transform.GetChild(1).gameObject.SetActive(true);
        } /*else if (_upgradeType is Cash) //Byt till CashPickUp
        {
            transform.GetChild(2).gameObject.SetActive(true);
        } else if (_upgradeType is Repair) //Byt till RepairPickUp
        {
            transform.GetChild(3).gameObject.SetActive(true);
        } */
        else if (_upgradeType is RefuelTurboPickUp)
        {
            transform.GetChild(3).gameObject.SetActive(true);
        } /* else if (_upgradeType is Mine)
        {
            transform.GetChild(5).gameObject.SetActive(true);
        } */
    }

    private PickUp SetRandomPickUp()
    {
        return UpgradeTypes[Random.Range(0, UpgradeTypes.Count)];
    }

	void Start () {
        UpgradeCollider = GetComponent<BoxCollider>();
	}
	
	void Update () {
        UpdateCollision();
    }

    private void UpdateCollision()
    {
        Collider[] hits = Physics.OverlapBox(UpgradeCollider.center, UpgradeCollider.bounds.extents, UpgradeCollider.transform.rotation, CarLayer);

        foreach (Collider col in hits)
        {
            if (col.GetComponent<Car>() != null)
            {
                
                col.GetComponent<Car>().EquipUpg(_upgradeType);
                ObjectPool.Destroy(this);
                return;
            }
        }
    }
}
