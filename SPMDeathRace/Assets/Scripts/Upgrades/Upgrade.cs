﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Upgrade : ScriptableObject {

    [HideInInspector] public Car Owner;
    public abstract void OnEquip();
    public abstract void OnUnEquip();
    [HideInInspector] public Upgrade originalUpgrade = null;
    public bool SaveItem = false;
    public Upgrade Equip(Car owner)
    {
        Upgrade original = originalUpgrade == null ? this : originalUpgrade;
        Upgrade instance = Instantiate(original);
        instance.originalUpgrade = original;
        instance.Owner = owner;
        instance.OnEquip();
        return instance;
    }

}
