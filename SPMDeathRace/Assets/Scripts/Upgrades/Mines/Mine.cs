﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Debuff/Mine")]
public class Mine : PickUp {
    public float SpeedDown;
    private float OriginalSpeed;
    public float TimeToSpeedDown = 2;
    public float TimeToSpeedUp;
    private float _speedDowntimer;
    private float _speedUptimer;
    private bool SpeedingDown = true;
    public override void OnEquip()
    {
        Owner.UpdateCarDelegate += SlowDown;
        OriginalSpeed = Owner.OriginalSpeed;
        Debug.Log("Equipping MineDebuff");
        
    }

    public override void OnUnEquip()
    {
        Owner.Speed = OriginalSpeed;
        Owner.UpdateCarDelegate -= SlowDown;
    }
    public void SlowDown()
    {
        Debug.Log("MineDebuff");
        if (SpeedingDown)
        {
            if (_speedDowntimer < TimeToSpeedDown)
                _speedDowntimer += Time.deltaTime;
            else
                SpeedingDown = false;
            Owner.Speed = Mathf.Lerp(OriginalSpeed, OriginalSpeed - SpeedDown, _speedDowntimer / TimeToSpeedDown);
        }
        else
        {
            if (_speedUptimer < TimeToSpeedUp)
                _speedUptimer += Time.deltaTime;
            else
                Owner.UnequipUpg(this);
            Owner.Speed = Mathf.Lerp(OriginalSpeed - SpeedDown, OriginalSpeed, _speedUptimer / TimeToSpeedUp);
        }

    }
}
