﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TrackTool : MonoBehaviour
{
    // Debug Trash
    public GameObject debugPrefab;
    public GameObject debugPrefab2;
    public GameObject checkpoint;
    public LineRenderer debugRenderer;

    ///////

    // Attributes
    public float toCloseToBeAPoint;
    public LayerMask raycastLayer;
    public Track track;

    private bool nodeMode, pickupMode, checkpointMode;
    private TrackNode previous;
    private GameObject _dynamicObjects;

    // Methods
    private void Start()
    {
        // Debug
        debugRenderer = GetComponent<LineRenderer>();
        debugRenderer.positionCount = 0;

        _dynamicObjects = new GameObject();

        if (track.nodes == null)
            track.nodes = new List<TrackNode>();

        if (track.trackPoints == null)
            track.trackPoints = new KDTree();
        if (track.nodes.Count != 0)
            previous = track.nodes[track.nodes.Count - 1];
    }

    private void OnApplicationQuit()
    {
        track.CalculateDistancesToGoalForEachNodeInTheTree();
    }

    void Update()
    {
        if (nodeMode)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                DrawTrack();
            }
        }
        else if (pickupMode)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                PlacePickup();
            }
        }
    }

    public void NodeMode()
    {
        nodeMode = true;
        pickupMode = false;
        checkpointMode = false;
    }

    public void PickupMode()
    {
        nodeMode = false;
        pickupMode = true;
        checkpointMode = false;
    }

    public void CheckpointMode()
    {
        nodeMode = false;
        pickupMode = false;
        checkpointMode = true;
    }

    public void PlacePickup() {

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, raycastLayer))
        {
            DebugMeToo(hit.point);
        }
    }

    public void Reset()
    {

        if (Application.isPlaying)
        {
            Destroy(_dynamicObjects);
            _dynamicObjects = new GameObject();
            debugRenderer.positionCount = 0;
        }
        track.nodes = new List<TrackNode>();
        track.trackPoints = new KDTree();
    }

    public void DrawTrack()
    {
        RaycastHit hit;

        // layermask"!!
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, raycastLayer))
        {
            TrackNode trackPoint = new TrackNode(hit.point.x, hit.point.z);

            if (track.trackPoints.IsEmpty())
            {
                DebugMe(hit.point);
                previous = track.trackPoints.Insert(trackPoint);
                track.nodes.Add(trackPoint);
                return;
            }

            if (Distance(previous, trackPoint) < toCloseToBeAPoint)
                return;

            trackPoint.Previous = previous;
            previous.SetNextNode(trackPoint);
            previous = trackPoint;
            track.trackPoints.Insert(trackPoint);
            track.nodes.Add(trackPoint);

            DebugMe(hit.point);
        }
    }

    private void DebugMe(Vector3 point)
    {
        // Debug
        debugRenderer.positionCount = debugRenderer.positionCount + 1;
        debugRenderer.SetPosition(debugRenderer.positionCount - 1, point);
        Instantiate(debugPrefab, point, Quaternion.identity, _dynamicObjects.transform);
    }

    private void DebugMeToo(Vector3 point)
    {
        // Debug
        Instantiate(debugPrefab2, point, Quaternion.identity, _dynamicObjects.transform);
    }

    public static float Distance(TrackNode first, TrackNode second)
    {
        return Vector2.Distance(new Vector2(first.X, first.Y), new Vector2(second.X, second.Y));
    }
}
