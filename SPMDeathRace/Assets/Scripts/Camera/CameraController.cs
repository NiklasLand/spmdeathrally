﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using System.Linq;

public class CameraController : MonoBehaviour
{
    public float yOffset = 25f;
    public float cameraLerpSpeed = 3f;
    public float lookAheadDistance = 3f;
    public float playerVelocityForwardWeight = 2f;
    [Header("Other car weights")]
    public float otherCarDetectionRange = 18f;
    public float otherCarsWeight = 1.5f;
    

    [HideInInspector] public static CameraController instance;

    private Transform mainTarget;
    private List<PlayerController> otherPlayers;
    private Vector3 nextNode; 
    private Vector3 previousNode; 
    private Vector3 previousLookDirection;

    private RippleEffect rippleEffect;
    private ChromaticAberrationModel chromaticAbberation;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        rippleEffect = GetComponentInChildren<RippleEffect>();
        chromaticAbberation = GetComponentInChildren<PostProcessingBehaviour>().profile.chromaticAberration;

        otherPlayers = new List<PlayerController>(); 
        otherPlayers.AddRange(FindObjectsOfType<PlayerController>());

        if (PlayerController.LocalVersion != null)
            mainTarget = PlayerController.LocalVersion.transform;
        else
            return;
        
        if(TrackManager.instance == null)
        {
            nextNode = mainTarget.transform.position;
            previousNode = mainTarget.transform.position;
        }
        else
        {
            nextNode = TrackManager.instance.track.GetNearestNextPoint(new Vector2(mainTarget.position.x, mainTarget.position.z));
            previousNode = mainTarget.transform.position;
        }

       
    }

    private void Update()
    {
        if (PlayerController.LocalVersion == null)
            return;
        else if(mainTarget == null)
            mainTarget = PlayerController.LocalVersion.transform;

        GetNextTrackPointNode();

        Vector3 playerToAverageCarDirecetion = GetOtherCarWeights();

        Vector3 playerToNextPointDirection = GetNextPointDirection();

        Vector3 finalCameraLookPoint = mainTarget.position + playerToAverageCarDirecetion + playerToNextPointDirection + (PlayerController.LocalVersion.transform.forward * playerVelocityForwardWeight);
        finalCameraLookPoint.y = yOffset;

        transform.position = Vector3.Lerp(transform.position, finalCameraLookPoint, cameraLerpSpeed * Time.deltaTime);
    }

    private Vector3 GetOtherCarWeights()
    {
        int otherCarCount = 0;
        Vector3 averageCarWeight = Vector3.zero;

        if(otherPlayers.Count < 4)
        {
            List<PlayerController> tempOtherPlayers = FindObjectsOfType<PlayerController>().ToList();
            foreach(PlayerController player in tempOtherPlayers)
            {
                if (!otherPlayers.Contains(player))
                    otherPlayers.Add(player);
            }
        }

        foreach (PlayerController otherCar in otherPlayers)
        {
            if (Vector3.Distance(mainTarget.position, otherCar.transform.position) < otherCarDetectionRange)
            {
                otherCarCount++;
                averageCarWeight += otherCar.transform.position;
            }
        }

        Vector3 playerToAverageCarDirection = Vector3.zero;

        if (otherCarCount > 0)
        {
            averageCarWeight /= otherCarCount;
            playerToAverageCarDirection = (averageCarWeight - mainTarget.position).normalized;
            playerToAverageCarDirection *= otherCarsWeight;
        }

        return playerToAverageCarDirection;
    }

    private Vector3 GetNextPointDirection()
    {
        Vector3 prevNodeToNextNodeDirection = (nextNode - previousNode).normalized;
        prevNodeToNextNodeDirection += previousLookDirection;
        prevNodeToNextNodeDirection /= 2;
        return prevNodeToNextNodeDirection * lookAheadDistance;
    }

    private void GetNextTrackPointNode()
    {
      if(TrackManager.instance != null)
 
        {
            Vector3 tempNextNode = TrackManager.instance.track.GetNearestNextPoint(new Vector2(mainTarget.position.x, mainTarget.position.z));
            if (tempNextNode != nextNode)
            {
                previousLookDirection = (nextNode - previousNode).normalized;
                previousNode = nextNode;
                nextNode = tempNextNode;
            }
        }
    }

    public void AddCameraShake(float intensity, float duration)
    {
        CameraShake.Shake(intensity, duration);
    }

    public void AddCameraWobble(float duration)
    {
        rippleEffect.enabled = true;
        chromaticAbberation.enabled = true;
        StartCoroutine(WobbleEffect(duration));
        
    }

    private IEnumerator WobbleEffect(float duration)
    {
        yield return new WaitForSeconds(duration);
        rippleEffect.enabled = false;
        chromaticAbberation.enabled = false;
    }

}
