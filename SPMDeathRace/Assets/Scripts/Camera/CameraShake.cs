﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

	public AnimationCurve IntensityToMagnitudeCurve;
	public Vector2 PerlinSpeed;
	public Vector2 MaxShake;
	public float RollSpeed;
	public float MaxRoll;

	private static float _intensity = 0;
    private static float _maxDuration;
    private static bool _shake = false;
    public static void Shake(float intensity, float duration) {
		_intensity += intensity;
        _maxDuration = duration;
        _shake = true;
    }

	private void Update() {
        if (!_shake)
            return;
		_intensity -= Time.deltaTime / _maxDuration;
		_intensity = Mathf.Clamp01(_intensity);

		float magnitude = IntensityToMagnitudeCurve.Evaluate(_intensity);
		float xPerlin = Mathf.Lerp(-MaxShake.x, MaxShake.x,
			                Mathf.PerlinNoise(Time.time * PerlinSpeed.x, 0f));
		float yPerlin = Mathf.Lerp(-MaxShake.y, MaxShake.y,
			                Mathf.PerlinNoise(0.0f, Time.time * PerlinSpeed.y));
		float roll = Mathf.Lerp(-MaxRoll, MaxRoll,
			             Mathf.PerlinNoise(Time.time * RollSpeed, Time.time * RollSpeed));

		transform.localPosition = new Vector3(xPerlin, yPerlin, 0.0f) * magnitude;
		transform.localRotation = Quaternion.Euler(0.0f, 0.0f, roll * magnitude);
        if (_intensity <= 0f)
            _shake = false;
	}
}
