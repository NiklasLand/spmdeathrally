﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Skidmark : MonoBehaviour
{

    private LineRenderer skidmark;
    private float timer1 = 0f;
    private int count1 = 1;
    private bool StopUpdating = false;
    private List<Vector3> path = new List<Vector3>();
    public float TimeBetweenSkids = .1f;
    public PlayerController pc;
    public int WheelIndex;
    private Wheel Wheel
    {
        get
        {
            return pc.AllWheels[WheelIndex];
        }
    }

    void Start ()
    {
        path.Add(Wheel.WheelTransform.position);
        skidmark = GetComponent<LineRenderer>();
        timer1 = TimeBetweenSkids;
    }

    void Update () 
    {
        if (StopUpdating) return;
        timer1 -= Time.deltaTime;

        if (timer1 <= 0 && Wheel.IsSkidding)
        {
            count1++;
            path.Add(Wheel.WheelTransform.position);
            skidmark.positionCount = count1;
            skidmark.SetPositions(path.ToArray());
            timer1 = TimeBetweenSkids;

        }
        if (!Wheel.IsSkidding)
        {
            StopUpdating = true;
            StartCoroutine(DestroySkidmark());
        }


    }


    public IEnumerator DestroySkidmark()
    {
        yield return new WaitForSeconds(20f);
        Destroy(this.gameObject);


        yield return (0f);
    }
}

