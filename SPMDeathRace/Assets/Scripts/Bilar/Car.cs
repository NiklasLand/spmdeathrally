﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public abstract class Car : ScriptableObject {
    public string CarName, ModelName, WeaponName, SpecialName;
    public float Speed = 100, Turbo, Acceleration, WeaponRange, AttackCooldown, CurrenHealth;
    protected float AttackCooldownTimer;
    public int Cost, Armour, MaxHealth, Damage, CollisionDamage;
    protected bool Invulnerable = false;
    public CarStats LobbyUiData;
    private bool IsInTurbo;
    private float TurboSpeed = 150;
    [HideInInspector] public float OriginalSpeed = 100;

    [Header("Prefab")]
    public GameObject VisualPrefab;
    [HideInInspector] public PlayerController MyController;
    public virtual void Initialize(PlayerController controller)
    {
        MyController = controller;
        UpdateCarDelegate += AttackCooldownTimerUpdate;
        OriginalSpeed = Speed;
        TurboSpeed = Speed * 1.5f;
    }
    public virtual void TakeDamage(int damage)
    {
        
        if (Invulnerable) return;
        CurrenHealth -= GetDamageTakenAfterArmourMitigation(damage);
        
    }
    private float GetDamageTakenAfterArmourMitigation(float damage)
    {
        float damgeAfterMitigation = damage - damage * Armour / 100f;
        return damgeAfterMitigation;
    }

    //TODO fixa så att denna metod hämtar den faktiska skjutpunkten
    public Vector3 GetShootPointTransfrom()
    {
        return MyController.GetComponentInChildren<PickMesh>().transform.Find("FiringPoint").transform.position;
        //return MyController.transform.Find("FiringPoint").transform.position;
    }
    public void TurboBoost(bool turbo)
    {
        if(!turbo || Turbo <= 0)
        {
            Speed = OriginalSpeed;
        }
        else if (Turbo > 0 && turbo)
        {
            Speed = TurboSpeed;
            Turbo -= Time.deltaTime;
        }
    }
    public delegate void UpdateCar();
    public void AttackCooldownTimerUpdate()
    {
        AttackCooldownTimer += Time.deltaTime;
    }
    public UpdateCar UpdateCarDelegate;
    public List<Upgrade> StartUpgrades;
    protected List<Upgrade> Upgrades = new List<Upgrade>();
    public List<Upgrade> UpgradeListForUI = new List<Upgrade>();
    public virtual void EquipUpg(Upgrade upg)
    {
        Upgrade temp = upg.Equip(this);
        if(upg.SaveItem) UpgradeListForUI.Add(upg);
        Upgrades.Add(temp);
    }
    [ContextMenu("Equipp StartingUpgs")]
    public void EqupiStartingUpgrades()
    {
        foreach(Upgrade u in StartUpgrades)
        {
            EquipUpg(u);
        }
    }
    public virtual void UnequipUpg(Upgrade upg)
    {
        upg.OnUnEquip();
        Upgrades.Remove(upg);
        Destroy(upg);
    }
    public abstract void Fire();
    public abstract void Special();

    public List<Upgrade> GetAllUpgrades()
    {
        return Upgrades;
    }
    public void GetAllUpgrades(List<Upgrade> upIn)
    {
        foreach(Upgrade u in upIn)
        {
            EquipUpg(u);
        }
    }
}
