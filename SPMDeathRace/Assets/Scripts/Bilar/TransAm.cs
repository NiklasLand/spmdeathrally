﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CarType/TransAm")]
public class TransAm : Car {
    public float CurrentEnergy;
    private float MaxEnergy = 100f;
    public float LaserCost;
    public float ShieldCost;
    public float ShieldTime;
    public float LazerBredth;
    private float _shieldTimer;
    private bool _isShielded;
    public GameObject LazerParticle;
    public GameObject LazerLineRenderer;
    private List<Car> hitList = new List<Car>();
    private GameObject LazerRendrerInstance;
    private GameObject LazerParticleInstance;
    public GameObject ShieldPrefab;
    private GameObject ShieldPrefabInstance;
    public float LazerDuration;
    private float LazerTimer;
    private bool IsFireing = false;
    public override void Initialize(PlayerController controller)
    {
        base.Initialize(controller);
        UpdateCarDelegate += RechargeLazer;
    }
    public override void Fire()
    {
        Debug.Log("TransAm Fire is called");
        if (AttackCooldownTimer < AttackCooldown) return;
        if (IsFireing) return;
        if (CurrentEnergy >= LaserCost)
        {
            IsFireing = true;
            if (!MyController.hasAuthority)
            {
                Debug.Log("TransAm Fire is not local");
                LazerTimer = 0;
                UpdateCarDelegate += UpdateLazerTimer;
                LaserGraphics();
                return;
            }
            Debug.Log("TransAm Fire is local");
            LazerTimer = 0;
            UpdateCarDelegate += UpdateLazerTimer;
            LaserGraphics();
            UpdateCarDelegate += UpdateShootLaser;
            CurrentEnergy -= 2f;
        }
        else
        {
            //Give feedback to user regarding low resources
        }
    }
    public void RechargeLazer()
    {
        CurrentEnergy += (MaxEnergy / 10f) * Time.deltaTime;
        if (CurrentEnergy > MaxEnergy) CurrentEnergy = MaxEnergy;
    }
    public void LaserGraphics()
    {
        SoundManager.instance.PlayOneShot(SoundManager.OneShots.WeaponLaser, MyController.transform.position);
        LazerRendrerInstance = ObjectPool.Instantiate(LazerLineRenderer, GetShootPointTransfrom(),MyController.transform.rotation);
        //LazerParticleInstance = ObjectPool.Instantiate(LazerParticle, GetShootPointTransfrom(),MyController.transform.rotation);
     
        UpdateCarDelegate += UpdateShootGraphics;
    }
    public void UpdateLazerTimer()
    {
        LazerTimer += Time.deltaTime;
        if (LazerTimer > LazerDuration)
        {
            UpdateCarDelegate -= UpdateLazerTimer;
            IsFireing = false;
        }
           
    }
    public void UpdateShootGraphics()
    {
        LazerRendrerInstance.transform.position = GetShootPointTransfrom();
        LazerRendrerInstance.GetComponent<LineRenderer>().SetPosition(0, GetShootPointTransfrom());
        Vector3 endPoint = GetShootPointTransfrom() + MyController.transform.forward * WeaponRange;
        LazerRendrerInstance.GetComponent<LineRenderer>().SetPosition(1, endPoint);
        if (LazerTimer > LazerDuration)
        {
            ObjectPool.Destroy(LazerRendrerInstance);
           // ObjectPool.Destroy(LazerParticleInstance);
            UpdateCarDelegate -= UpdateShootGraphics;
        }
    }
    private void UpdateShootLaser()
    {
        
        if(LazerTimer < LazerDuration)
        {
            if(AttackCooldownTimer > AttackCooldown)
            {
                CheckLazerCollision();
            }
            Mathf.Lerp(CurrentEnergy, 0, LazerTimer / LazerDuration);
        }
        else
        {
            CurrentEnergy = 0;
            UpdateCarDelegate -= UpdateShootLaser;
        }
    }
    public void CheckLazerCollision()
    {
        Vector3 center = GetShootPointTransfrom() + (MyController.transform.forward * WeaponRange) / 2;
        Vector3 halfExtends = new Vector3(LazerBredth / 2, LazerBredth / 2, WeaponRange / 2);
        Collider[] hits = Physics.OverlapBox(center, halfExtends, MyController.transform.rotation,MyController.CarLayer);
        foreach(Collider c in hits)
        {
            if (c.GetComponent<PlayerController>() == MyController) continue;
            MyController.CmdDamageOtherPlayer(Damage, c.GetComponent<PlayerController>().Id, c.GetComponent<PlayerController>().transform.position, c.GetComponent<PlayerController>().transform.position - MyController.transform.position,0f);
        }
    }

    private void ShieldUpdate()
    {
        _shieldTimer += Time.deltaTime;
        Mathf.Lerp(CurrentEnergy,0, _shieldTimer / ShieldTime);
        if (_shieldTimer > ShieldTime)
        {
            Invulnerable = false;
            _isShielded = false;
            ObjectPool.Destroy(ShieldPrefabInstance);
            _shieldTimer = 0;
            CurrentEnergy = 0;
            UpdateCarDelegate -= ShieldUpdate;
        }
    }

    public override void Special()
    {
        Debug.Log("Sheidl called");
        if(CurrentEnergy >= ShieldCost && !_isShielded)
        {
            Invulnerable = true;
            _isShielded = true;
            ShieldPrefabInstance = ObjectPool.Instantiate(ShieldPrefab,MyController.transform);
            ShieldPrefabInstance.transform.position = MyController.transform.position;
            UpdateCarDelegate += ShieldUpdate;
        }
        else
        {
            //Give feedback to user regarding low resources
        }
    }
}
