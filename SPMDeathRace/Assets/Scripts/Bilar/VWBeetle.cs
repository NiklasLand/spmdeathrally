﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "CarType/VWBeetle")]
public class VWBeetle : AmmoBasedCar
{
    
    public int MaxMines;
    public int CurrentMines;
    public GameObject pickUpPrefab;
    public PickUp Mine;
    public float ProjectileSpeed;
    public GameObject ShootParticleEffect;
    public override void Initialize(PlayerController controller)
    {
        base.Initialize(controller);
    }

    public override void Fire()
    {
        if(AttackCooldownTimer > AttackCooldown)
        {
            Shoot();
            AttackCooldownTimer = 0;
        }
    }
    private void Shoot()
    {
        if (AttackCooldownTimer < AttackCooldown) return;
        if (!MyController.isLocalPlayer)
        {
            GraphicalShoot();
            return;
        }
        if (CurrentAmmo > 0)
        {
            
            GraphicalShoot();
            ShootMinigun();
            CurrentAmmo -= 1;
        }
        else
        {
            //Give feedback to user regarding low resources
        }
        
    }
    private void GraphicalShoot()
    {
        SoundManager.instance.PlayOneShot(SoundManager.OneShots.WeaponMachinegun, MyController.transform.position);
        ObjectPool.Instantiate(ShootParticleEffect, GetShootPointTransfrom(), MyController.transform.rotation);
    }
    private void ShootMinigun()
    {
        Ray ray = new Ray();
        ray.direction = MyController.transform.forward;
        ray.origin = MyController.transform.position;
        RaycastHit[] hits = Physics.RaycastAll(ray,WeaponRange,MyController.CarLayer);
        foreach(RaycastHit hit in hits)
        {
            if (hit.collider.GetComponent<PlayerController>() == MyController) continue;
            MyController.CmdDamageOtherPlayer(Damage, hit.collider.GetComponent<PlayerController>().Id,hit.point,ray.direction, 0f);
            break;
        }
    }

    public override void Special()
    {
        if (CurrentMines > 0)
        {
            Vector3 tempPos = Vector3.zero; // CarControllers pos minus offest
            GameObject temp = ObjectPool.Instantiate(pickUpPrefab, tempPos, Quaternion.identity);
            temp.GetComponent<PhysicalUpgrade>().Initialize(Mine);
            CurrentMines -= 1;
        }
    }

}
