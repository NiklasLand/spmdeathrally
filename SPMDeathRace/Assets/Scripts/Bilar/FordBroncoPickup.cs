﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CarType/FordBroncoPickup")]
public class FordBroncoPickup : AmmoBasedCar
{
    public float HarpoonCooldown;
    private float HarpoonCooldownTimer;
    public float CannonBallSpeed;
    public float CannonBallKnockBackForce;
    public GameObject CannonBallPrefab;

    public override void Initialize(PlayerController controller)
    {
        base.Initialize(controller);
        UpdateCarDelegate += delegate
        {
            HarpoonCooldownTimer += Time.deltaTime;
        };

    }
    public override void Fire()
    {
        if (AttackCooldownTimer < AttackCooldown) return;
        if (!MyController.isLocalPlayer) return;
        
        if (CurrentAmmo > 0)
        {
            SoundManager.instance.PlayOneShot(SoundManager.OneShots.WeaponCannon, MyController.transform.position);
            GameObject tempCannonBall  = ObjectPool.Instantiate(CannonBallPrefab, MyController.transform.position,Quaternion.identity);
            tempCannonBall.GetComponent<CannonBallLogic>().Initilize(MyController.transform.forward, CannonBallSpeed, Damage, WeaponRange, CannonBallKnockBackForce, this);
            CurrentAmmo -= 1;
            AttackCooldownTimer = 0;
        }
        else
        {
            //Give feedback to user regarding low resources
        }
        
    }

    public override void Special()
    {
       if(HarpoonCooldownTimer > HarpoonCooldown)
        {
            HarpoonCooldownTimer = 0;
            // Shoot harpoon
        }
    }
}
