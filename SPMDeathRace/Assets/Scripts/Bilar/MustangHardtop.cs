﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CarType/MustangHardtop")]
public class MustangHardtop : AmmoBasedCar
{
    public float ShotgunAngle;
    public float ShotgunKnockbackForce;
    public GameObject ShotGunGrapicEffect;
    public int DamageZoneCount = 3;
    public override void Fire()
    {
        if (AttackCooldownTimer < AttackCooldown) return;
        if (!MyController.isLocalPlayer)
        {
            ShootGraphics();
            return;
        }
        if (CurrentAmmo > 0)
        {
            
            ShootLogic();
            ShootGraphics();
            AttackCooldownTimer = 0;
            //CurrentAmmo -= 1;
        }
        else
        {
            //Give feedback to user regarding low resources
        }
    }
    private void ShootLogic()
    {
        Debug.Log("Fire");
        Collider[] hits = ShotgunCircleCast();
        foreach(Collider c in hits)
        {
            PlayerController otherController = c.GetComponent<PlayerController>();
            if (otherController == null) continue;
            Vector3 delta = otherController.transform.position - MyController.transform.position;
            delta = new Vector3(delta.x, 0f, delta.z);
            Vector3 CorrectedPosition = new Vector3 (MyController.transform.forward.x, 0f, MyController.transform.forward.z);
            float dot = Vector3.Dot(CorrectedPosition, delta);
            if (dot < ShotgunAngle) continue;
            int damageModifier = 1;
            for(int i = 1; i < DamageZoneCount; i++)
            {
                float stage = i / DamageZoneCount;
                if (delta.magnitude < WeaponRange * stage)
                    damageModifier = (1 + 1 / DamageZoneCount) - ( i/ DamageZoneCount);
            }
            Debug.DrawLine(MyController.transform.position, c.transform.position,Color.cyan, 20f);
            //Please hit
            RaycastHit hit;
            Physics.Raycast(MyController.transform.position, delta, out hit, WeaponRange, MyController.CarLayer);
            Vector3 direction = (hit.point - MyController.transform.position);
            MyController.CmdDamageOtherPlayer(Damage * damageModifier, otherController.Id,hit.point, direction, ShotgunKnockbackForce);
        }
    }
    private void ShootGraphics()
    {
        SoundManager.instance.PlayOneShot(SoundManager.OneShots.WeaponShotgun, MyController.transform.position);
        ObjectPool.Instantiate(ShotGunGrapicEffect, GetShootPointTransfrom(), MyController.transform.rotation);
    }
    private Collider[] ShotgunCircleCast()
    {
        Collider[] tempHit = Physics.OverlapSphere(MyController.transform.position, WeaponRange, MyController.CarLayer);
        return tempHit;
    }

    public override void Special()
    {
        //Fråga designers
    }

    // Use this for initialization
    void Start () {
		
	}
	
}
