﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkidmarkManager : MonoBehaviour 
{
    public GameObject Skidmark;
    private PlayerController pc;
    private bool[] Skidding = new bool[4];

    void Start() 
    {
        pc = GetComponent<PlayerController>();
    }

    void Update () 
    {
        for (int i =0;i<pc.AllWheels.Length;i++) 
        {
            if (pc.AllWheels[i].IsSkidding && !Skidding[i]) 
            {
                Skidding[i] = true;
                GameObject skid = Instantiate(Skidmark);
                skid.GetComponent<Skidmark>().WheelIndex = i;
                skid.GetComponent<Skidmark>().pc = pc;
            }
            if(!pc.AllWheels[i].IsSkidding)
                Skidding[i] = false;
        }
    }
}

