﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrackNode
{
    public float distanceToGoal;
    public Vector2 position;
    public float x, y;
    private TrackNode nextNode, previousNode;

    public float X { get { return x; } }
    public float Y { get { return y; } }

    public TrackNode Previous { get { return previousNode; } set{ previousNode = value; } }

    public TrackNode(float x, float y)
    {
        this.x = x;
        this.y = y;
        position = new Vector2(x, y);
    }

    public Vector2 ToVector2()
    {
        return new Vector2(x, y);
    }

    public TrackNode GetNextNode()
    {
        return nextNode;
    }

    public void SetNextNode(TrackNode nextNode)
    {
        this.nextNode = nextNode;
    }

    public void SetNodeCoordinates(float x, float y)
    {
        this.x = x;
        this.y = y;

        Debug.Log(this.x + ", " + this.y);
    }

    public override string ToString()
    {
        return x + ", " + y;
    }
}
