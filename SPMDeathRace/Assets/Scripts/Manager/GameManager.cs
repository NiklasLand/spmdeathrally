﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : NetworkBehaviour {

    public static GameManager instance;

    public GameObject[] bots;
    [HideInInspector] public int readyPlayers = 0;
    public GameObject depotCanvas;
    public UIManager uiMan;
    public DepotMenu depot;

    [HideInInspector] public PlayerController[] players;

    private int[] playerIds;
    public int currentMap = 2;
    private bool depotTimesUp = false;
    int ReadyRequired = -1;

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    void Start () {

        if (instance == null) instance = this;

        if (isServer)
        {
            OverriddenNetworkLobbyPlayer[] networkPlayers = FindObjectsOfType<OverriddenNetworkLobbyPlayer>();

            SpawnAI(networkPlayers);

            playerIds = new int[networkPlayers.Length];
            ReadyRequired = 0;
            for (int i = 0; i < playerIds.Length; i++)
            {
                playerIds[i] = networkPlayers[i].Id;
                if (!networkPlayers[i].isBot)
                    ReadyRequired++;
            }

            players = FindObjectsOfType<PlayerController>();

            StartCoroutine(StartGame());
        }
	}

    private void Update()
    {
        if (!isServer)
            return;

        if (TrackManager.instance != null)
        {
            UpdateLeaderboard();
            RpcUpdatePlacementList(playerIds);
        }
    }
    private void SpawnAI(OverriddenNetworkLobbyPlayer[] networkPlayers)
    {
        foreach (OverriddenNetworkLobbyPlayer player in networkPlayers)
        {
            if (player.isBot)
            {
                GameObject bot = Instantiate(bots[Random.Range(0, bots.Length)]);
                bot.GetComponent<PlayerController>().Id = player.Id;
                NetworkServer.SpawnWithClientAuthority(bot, NetworkServer.connections[0]);
                Debug.Log("spawned bot");
            }
        }
    }

    [ClientRpc]
    private void RpcLoadLevel(int mapIndex)
    {
        if (mapIndex != 0)
            SceneManager.UnloadScene(mapIndex - 1);
        SceneManager.LoadScene(mapIndex, LoadSceneMode.Additive);
        Debug.Log("RpcLoadLevel");
    }

    private IEnumerator StartGame()
    {
        while (readyPlayers != ReadyRequired)
            yield return null;

        Debug.Log(readyPlayers + " ready players");
        players = FindObjectsOfType<PlayerController>();

        foreach (PlayerController player in players)
        {
            player.RpcTimeToSetIDs();
        }

        while (!GetPlayers())
            yield return null;

        RpcLoadLevel(currentMap++);

        RpcStartTimerTimeDown();

        yield return new WaitForSeconds(.9f);

        GameObject[] spawnpoint = new GameObject[0];
        while(spawnpoint.Length < players.Length)
        {
            spawnpoint = GameObject.FindGameObjectsWithTag("SpawnPoint");
            yield return null;
        }

        int num = 0;

        foreach(PlayerController player in players)
        {
            player.RpcMovePlayerToStartingLine(spawnpoint[num].transform.position, spawnpoint[num].transform.rotation);
            ++num;
        }

        finishedPlayers = 0;
        depotTimesUp = false;

        yield return new WaitForSeconds(5f);
        foreach(PlayerController player in players)
        {
            player.RpcCanDrive(true);
        }
        won = false;
    }

    [ClientRpc]
    private void RpcStartTimerTimeDown()
    {
        uiMan.StartRaceTimer();
    }

    private bool GetPlayers()
    {
        if (players.Length == playerIds.Length)
        {
            foreach (PlayerController player in players)
            {
                if (player.Id == -1)
                    return false;
            }
            return true;
        }
        players = FindObjectsOfType<PlayerController>();
        return false;
    }

    private void UpdateLeaderboard()
    {
        bool change = false;
        for (int i = 1; i < players.Length; i++)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                if (CompareSmaller(players[i], players[j]))
                {
                    change = true;
                    int temp = playerIds[i];
                    playerIds[i] = playerIds[j];
                    playerIds[j] = temp;
                }
            }
        }
        if (change)
            RpcUpdatePlacementList(playerIds);
    }

    [ClientRpc]
    private void RpcUpdatePlacementList(int[] playerIds)
    {
        this.playerIds = playerIds;

        for (int i = 0; i < playerIds.Length; i++)
        {
            for (int j = 0; j < players.Length; j++)
            {
                if (playerIds[i] == players[j].Id)
                {
                    PlayerController temp = players[i];
                    players[i] = players[j];
                    players[j] = temp;
                }
            }
        }
    }

    public int GetMyPlacement(int id)
    {
       for (int i = 0; i < players.Length; i++)
        {
            if (players[i].Id == id)
                return i + 1;
        }
        return -1;
    }

    private int finishedPlayers;

    private bool won = false;

    public void SomeoneWon()
    {
        if (!isServer)
            return;
        
        ++finishedPlayers;

        if (!won && finishedPlayers >= playerIds.Length - 1)
        {
            won = true;
            foreach(PlayerController player in players)
                player.RpcCanDrive(false);

            RpcCourseFinished();
        }
    }

    private bool firstTime = true;

    [ClientRpc]
    private void RpcCourseFinished()
    {
        StartCoroutineMethod();
    }

    private void StartCoroutineMethod()
    {
        StartCoroutine(Depoing());
    }

    private IEnumerator Depoing()
    {
        depotCanvas.SetActive(true);
        depot.MyEnable();
        depot.StartTimer();
        while (!depot.timesUp)
            yield return null;

        depotCanvas.SetActive(false);
        depotTimesUp = true;

        if (isServer)
            CoroutineStarter();
    }

    private void CoroutineStarter()
    {
        StartCoroutine(StartGame());
    }

    private bool CompareSmaller(PlayerController first, PlayerController second)
    {
        if (first.currentLap > second.currentLap)
            return true;
        else if (first.currentLap < second.currentLap)
            return false;
        else
            return TrackManager.instance.track.GetNearestNextNode(ToVector2(first.transform.position)).distanceToGoal < TrackManager.instance.track.GetNearestNextNode(ToVector2(second.transform.position)).distanceToGoal;
    }

    private Vector2 ToVector2(Vector3 position)
    {
        return new Vector2(position.x, position.z);
    }

    public PlayerController GetPlayer(int id)
    {
        foreach (PlayerController player in players)
        {
            if (player.Id == id)
                return player;
        }
        return null;
    }

}
