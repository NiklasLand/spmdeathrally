﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Track Manager", menuName = "Track", order = 1)]
public class Track : ScriptableObject
{
    public string objectName = "Track Manager";

    public Vector2 debugNearest;

    public KDTree trackPoints;
    public List<TrackNode> nodes;

    private void OnEnable()
    {
        trackPoints = new KDTree();

        TrackNode prev = nodes[nodes.Count - 1];

        foreach (TrackNode node in nodes)
        {
            trackPoints.Insert(node);
            prev.SetNextNode(node);
            node.Previous = prev;
            prev = node;
        }
    }

    public Vector3 GetNearestNextPoint(Vector2 position)
    {
        //ToDo: Good enough??
        TrackNode nearest = trackPoints.NearestNeighbour(position).GetNextNode();
        return new Vector3(nearest.X, 0.0f, nearest.Y);
    }

    public TrackNode GetNearestNextNode(Vector2 position)
    {
        return trackPoints.NearestNeighbour(position).GetNextNode();
    }

    public void CalculateDistancesToGoalForEachNodeInTheTree()
    {
        if (nodes.Count == 0)
            return;

        nodes[nodes.Count - 1].distanceToGoal = TrackTool.Distance(nodes[nodes.Count - 1], nodes[0]);
        
        for (int i = nodes.Count - 2; i >= 0; i--)
        {
            nodes[i].distanceToGoal = TrackTool.Distance(nodes[i], nodes[i + 1]) + nodes[i + 1].distanceToGoal;
        }
    }

    public float DistanceToGoal(Vector2 position)
    {
        TrackNode nearest = trackPoints.NearestNeighbour(position);

        return Vector2.Distance(new Vector2(nearest.GetNextNode().X, nearest.GetNextNode().Y), position) + nearest.GetNextNode().distanceToGoal;
    }
}
