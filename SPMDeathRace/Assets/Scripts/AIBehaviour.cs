﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehaviour : MonoBehaviour {

    PlayerController controller;
    public float rand;
    public int lookAheadRange;
    public int ignoreTooClosePoints;
    public LayerMask carHitBoxLayer;

    private Vector2 Position { get { return ToVector2(transform.position); } }
    private float stoppedTimer = 0.0f;
    private bool reversing = false;

    private void Awake()
    {
        controller = GetComponent<PlayerController>();
        randomLane = Random.Range(-rand, rand);
    }

    float timer = 0;
    float randomLane;

    private void Update()
    {
        if (TrackManager.instance == null)
            return;

        if (reversing)
            return;

        Drive();
        FuckupCorrection();

        BehaviourUpdate();

    }

    private void Drive()
    {
        timer += Time.deltaTime;
        if (timer > 1f)
        {
            randomLane += Random.Range(-1f, 1f);
            randomLane = Mathf.Clamp(randomLane, -rand, rand);
            timer = 0.0f;
        }

        TrackNode nextNode = TrackManager.instance.track.GetNearestNextNode(Position);

        for (int i = 0; i < ignoreTooClosePoints; i++)
            nextNode = nextNode.GetNextNode();

        for (int i = 0; i < lookAheadRange; i++)
            nextNode = nextNode.GetNextNode();

        Vector2 movingAverageDirection = (nextNode.ToVector2() - Position).normalized;
        nextNode = nextNode.Previous;

        for (int i = 0; i < lookAheadRange - 1; i++)
        {
            Vector2 dir = (nextNode.ToVector2() - Position).normalized;
            Vector2 dirnormal = new Vector2(dir.y, -dir.x);
            dir = (nextNode.ToVector2() + dirnormal * randomLane - Position).normalized;
            movingAverageDirection += dir;
            nextNode = nextNode.Previous;
        }

        Vector2 direction = movingAverageDirection.normalized;

        Vector2 rayDirection = Vector2.zero;

        for (int i = 0; i < lookAheadRange; i++)
        {

            RaycastHit camHit;
            Physics.Raycast(transform.position + transform.up * 10f, new Vector3(nextNode.X, transform.position.y, nextNode.Y) - (transform.position + transform.up * 10f), out camHit, float.MaxValue);

            if (camHit.point.magnitude < 0.1f)
                break;

            Debug.DrawLine(transform.position, camHit.point, Color.blue);
            //new Vector3(nextNode.X, transform.position.y, nextNode.Y) - transform.position
            RaycastHit hit;
            if (Physics.Raycast(transform.position, camHit.point - transform.position, out hit, float.MaxValue))
            {
                if (ApproximatelyVector2(ToVector2(hit.point), nextNode.ToVector2()))
                {
                    rayDirection = ToVector2(hit.point - transform.position).normalized;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        if (rayDirection != Vector2.zero)
        {
            float directionDot = Vector2.Dot(ToVector2(transform.right), direction);
            float raycastDot = Vector2.Dot(ToVector2(transform.right), rayDirection);
            if (Mathf.Abs(raycastDot) < Mathf.Abs(directionDot))
            {
                Debug.Log("försökte fuska");
                direction = rayDirection;
            }
        }

        Debug.DrawLine(new Vector3(Position.x, 0, Position.y), TrackManager.instance.track.GetNearestNextPoint(Position), Color.yellow);

        float maxAngle = 0;
        int wheels = 0;

        foreach (Wheel w in controller.AllWheels)
        {
            if (w.CanTurn)
            {
                maxAngle += Mathf.Abs(w.MaxTurningDegree);
                wheels++;
            }
        }
        if (wheels == 0)
            return;

        maxAngle /= wheels;

        Vector2 speedLook = (nextNode.ToVector2() - Position).normalized;
        nextNode = nextNode.GetNextNode();

        for (int i = 0; i < lookAheadRange; i++)
        {
            speedLook += (nextNode.ToVector2() - Position).normalized;
            speedLook /= 2;
            nextNode = nextNode.GetNextNode();
        }

        float inputX = Mathf.Clamp(Vector2.Dot(ToVector2(transform.right), direction) * (90 / maxAngle), -1f, 1f);
        float curveSpeedDot = Vector2.Dot(ToVector2(transform.forward), speedLook.normalized);

        float currentSpeedFactor = (controller.Body.velocity.magnitude + 0.01f) / controller.GetState<OriginalState>().MaxSpeed;

        float inputY = Mathf.Clamp(Mathf.Clamp01(((curveSpeedDot - .5f) * 2) - currentSpeedFactor), -1f, 1f);

        // inputY = Mathf.Round(inputY * 2) / 2f;

        Vector2 input = new Vector2(inputX, inputY);

        if (input.y < Mathf.Abs(controller.InputRequiredToMove))
            input.y = controller.InputRequiredToMove;

        if (curveSpeedDot > 0.9f)
            input.y = 1.0f;

        controller.InputAxis = input;
    }

    private void FuckupCorrection()
    {
        if (!controller.CanDrive)
            return;
        if (controller.Body.velocity.magnitude < controller.Car.Speed * .01f)
            stoppedTimer += Time.deltaTime;
        else
            stoppedTimer = 0.0f;

        if (stoppedTimer > 1f)
        {
            StartCoroutine(Reverse());
            stoppedTimer = 0.0f;
        }
    }

    private void BehaviourUpdate()
    {
        UpdateShootBehaviour();
        UpdateOtherStuff();
    }

    private void UpdateOtherStuff()
    {
        if (controller.InputAxis.x < 0.2)
            controller.Buttons[0] = true;
    }
    
    private void UpdateShootBehaviour()
    {
        if(controller.Car is AmmoBasedCar)
        {
            if (((AmmoBasedCar)controller.Car).CurrentAmmo <= 0)
            {
                controller.Buttons[1] = false;
                return;
            }
        }
        else if (controller.Car is TransAm)
        {
            if (((TransAm)controller.Car).CurrentEnergy <= 20)
            {
                controller.Buttons[1] = false;
                return;
            }
        }

        PlayerController target = null;
        float distanceFromUs = Mathf.Infinity;

        float distanceToGoal = TrackManager.instance.track.GetNearestNextNode(ToVector2(transform.position)).distanceToGoal;

        //TODO: foreach (PlayerController player in GameManager.instance.players)
        foreach (PlayerController player in FindObjectsOfType<PlayerController>())
        {
            if (PlayerIsUs(player))
                continue;

            RaycastHit hit;

            if (Physics.Raycast(transform.position, player.transform.position - transform.position, out hit, float.MaxValue, carHitBoxLayer))
            {
                if (hit.transform.GetComponent<PlayerController>() == null)
                {
                    continue;
                }
                    
            }
                

            Vector2 otherPlayerPos = ToVector2(player.transform.position);

            if (TrackManager.instance.track.GetNearestNextNode(otherPlayerPos).distanceToGoal < distanceToGoal && Vector2.Distance(otherPlayerPos, ToVector2(transform.position)) < distanceFromUs)
            {
                distanceFromUs = Vector2.Distance(otherPlayerPos, ToVector2(transform.position));
                target = player;
            }
        }

        if (target == null)
        {
            controller.Buttons[1] = false;
            return;
        }
           

        Vector2 trackDirection = ToVector2(TrackManager.instance.track.GetNearestNextPoint(ToVector2(target.transform.position))) - ToVector2(TrackManager.instance.track.GetNearestNextPoint(ToVector2(transform.position)));
        trackDirection.Normalize();


        Vector2 toTargetVector = ToVector2(target.transform.position - transform.position);
        float distanceToTargetOnTrackVector = Vector2.Dot(toTargetVector, trackDirection);

        // TODO: if (distanceToTargetOnTrackVector > controller.Car.weaponRange)
        if (distanceToTargetOnTrackVector > 15f)
        {
            controller.Buttons[1] = false;
            return;
        }

        // TODO: if (Vector2.Dot(toTargetVector, ToVector2(transform.forward)) < controller.Car.weaponRange)
        if (Vector2.Dot(toTargetVector, ToVector2(transform.forward)) > .5f)
        {
            Debug.Log("shooting");
            controller.Buttons[1] = true;
        }

        if (distanceToTargetOnTrackVector < 4f)
        {
            bool hasStartedToPassOtherCar = false;

            if (toTargetVector.magnitude - distanceToTargetOnTrackVector > 4f)
                return;

            RaycastHit left, right;
            Physics.Raycast(transform.position, -transform.right, out left, float.MaxValue);
            Physics.Raycast(transform.position, transform.right, out right, float.MaxValue);

            float inputX = left.point.magnitude < 0.1f ? right.point.magnitude < 0.1f ? controller.InputAxis.x : 1f : right.point.magnitude < 0.1f ? -1f : (left.point - transform.position).magnitude < (right.point - transform.position).magnitude ? 1f : -1f;

            controller.InputAxis = new Vector2(inputX, controller.InputAxis.y);
            hasStartedToPassOtherCar = true;

            if (hasStartedToPassOtherCar)
            {
                controller.InputAxis = new Vector2(inputX, controller.InputAxis.y);
            }
        }
        else
        {
            float maxAngle = 0;
            int wheels = 0;

            foreach (Wheel w in controller.AllWheels)
            {
                if (w.CanTurn)
                {
                    maxAngle += Mathf.Abs(w.MaxTurningDegree);
                    wheels++;
                }
            }
            if (wheels == 0)
                return;

            maxAngle /= wheels;

            float inputX = Mathf.Clamp(Vector2.Dot(ToVector2(transform.right), toTargetVector.normalized) * (90 / maxAngle), -1f, 1f);
            controller.InputAxis = new Vector2(inputX, controller.InputAxis.y);
        }
    }

    private bool PlayerIsUs(PlayerController player)
    {
        return ApproximatelyVector2(ToVector2(player.transform.position), ToVector2(transform.position));
    }

    private IEnumerator Reverse()
    {
        reversing = true;

        float time = 0.0f;
        while (time < 1.0f)
        {
            controller.InputAxis = new Vector2(0, -1);
            time += Time.deltaTime;
            yield return null;
        }
        reversing = false;
    }

    private Vector2 ToVector2(Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }

    private bool ApproximatelyVector2(Vector2 first, Vector2 second)
    {
        return Mathf.Abs(first.x - second.x) < 1f && Mathf.Abs(first.y - second.y) < 1f;
    }

}
