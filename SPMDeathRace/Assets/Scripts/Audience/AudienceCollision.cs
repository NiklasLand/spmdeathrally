﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceCollision : MonoBehaviour {

    private Animator anim;
    public Rigidbody RootRigidbody;
    public SkinnedMeshRenderer Mesh;
    
    public GameObject BloodSplatter;
    
	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        BloodSplatter.SetActive(false);
        
	}

    // Update is called once per frame
    private void OnCollisionEnter(Collision col)
    {
        //if(col.gameObject.layer == LayerMask.NameToLayer("Car")) // 8)
        //{
            Mesh.enabled = false;
            BloodSplatter.SetActive(true);
            GetComponent<CapsuleCollider>().enabled = false;
            StartCoroutine(DestroyOnDeath());
        //}
        
        
    }

    private IEnumerator DestroyOnDeath()
    {
        yield return new WaitForSeconds(4);
        Destroy(gameObject);
    }
}
