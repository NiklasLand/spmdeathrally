﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TrackTool))]
public class TrackToolEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();


        TrackTool tool = (TrackTool)target;

        if (GUILayout.Button("Create Nodes"))
        {
            tool.NodeMode();
        }

        if (GUILayout.Button("Create Pickup Points"))
        {
            tool.PickupMode();
        }

        if (GUILayout.Button("Create Checkpoints"))
        {
            tool.CheckpointMode();
        }


        if (GUILayout.Button("RESET"))
        {
            tool.Reset();
        }
    }

}
