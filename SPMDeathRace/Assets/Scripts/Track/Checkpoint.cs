﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public CheckpointManager checkpointManager;


    private void OnTriggerEnter(Collider other)
    {
        PlayerController playerController = other.gameObject.GetComponent<PlayerController>();

        if (other.CompareTag("Player"))
        {
            string checkpointTag = this.tag;

            switch (checkpointTag)
            {
                case "Start Line":

                    checkpointManager.StartNewLap(other.gameObject);
                    checkpointManager.RegisterCheckpointEntry(this, other.gameObject);
                    playerController.marker1Passed = false;
                    playerController.marker2Passed = false;
                    Debug.Log("Start Line Entry!");
                    break;

                case "Checkpoint":

                    checkpointManager.RegisterCheckpointEntry(this, other.gameObject);
                    Debug.Log("Normal Checkpoint Registry");
                    break;

                case "Marker Checkpoint 1":

                    if (playerController.marker1Passed == false && playerController.marker2Passed == false)
                    {
                        checkpointManager.RegisterCheckpointEntry(this, other.gameObject);
                        playerController.marker1Passed = true;
                        Debug.Log("Marker 1 Checkpoint Registry");
                    }
                    break;

                case "Marker Checkpoint 2":

                    if (playerController.marker1Passed == true && playerController.marker2Passed == false)
                    {
                        checkpointManager.RegisterCheckpointEntry(this, other.gameObject);
                        playerController.marker2Passed = true;
                        Debug.Log("Marker 2 Checkpoint Registry");
                    }
                    break;
            }
        }
    }
}
