﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Checkpoint Manager", menuName = "Checkpoint Manager", order = 1)]
public class CheckpointManager : ScriptableObject
{
    public int requiredCheckpoints;
    public int requiredNumberOfLaps;

    private Dictionary<Checkpoint, List<GameObject>> checkpointCarPairs = new Dictionary<Checkpoint, List<GameObject>>();

    public void RegisterCheckpointEntry(Checkpoint checkpoint, GameObject carObject)
    {
        PlayerController playerController = carObject.GetComponent<PlayerController>();

        if (!checkpointCarPairs.ContainsKey(checkpoint))
        {
            checkpointCarPairs.Add(checkpoint, new List<GameObject>());
            checkpointCarPairs[checkpoint].Add(carObject);
            playerController.checkpointCounter++;
        }

        else if (checkpointCarPairs[checkpoint].Contains(carObject)) { return; }

        else
        {
            checkpointCarPairs[checkpoint].Add(carObject);
            playerController.checkpointCounter++;
        }
    }

    public void StartNewLap(GameObject carObject)
    {
        PlayerController carController = carObject.GetComponent<PlayerController>();

        if (carController.checkpointCounter >= requiredCheckpoints)
        {
            ClearCarFromCheckpoints(carObject);
            carController.currentLap += 1;
        }

        if (carController.currentLap > requiredNumberOfLaps)
        {
            DebugWin(carObject);
            //UnityEditor.EditorApplication.isPaused = true;
        }
    }

    private void ClearCheckpoint(Checkpoint checkpoint)
    {
        if (!checkpointCarPairs.ContainsKey(checkpoint)) { return; }

        else { checkpointCarPairs[checkpoint].Clear(); }
    }

    private void ClearCarFromCheckpoints(GameObject carObject)
    {
        PlayerController playerController = carObject.GetComponent<PlayerController>();

        foreach (KeyValuePair<Checkpoint, List<GameObject>> checkpointCarList in checkpointCarPairs)
        {
            if (checkpointCarList.Value.Contains(carObject))
            {
                checkpointCarList.Value.Remove(carObject);
                playerController.checkpointCounter = 0;
            }
        }
    }

    private void DebugWin(GameObject carObject)
    {
        Debug.Log(carObject.name + " has just won in spectacular fashion! Well Done!");
        GameManager.instance.SomeoneWon();
    }
}
