﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlayerController : MonoBehaviour {

    public int checkpointCounter;
    public int currentLap;
    public bool marker1Passed;
    public bool marker2Passed;

	void Start () { checkpointCounter = 0; currentLap = 1; marker1Passed = false; marker2Passed = false; }
}
