﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public abstract class Controller : NetworkBehaviour
{
    [SerializeField]
    private State[] _states;
    private readonly Dictionary<Type, State> _statesDictionary = new Dictionary<Type, State>();
    

    public State CurrentState;

    private void Awake()

    {


        foreach (State state in _states)
        {
            State stateCopy = Instantiate(state);
            stateCopy.Initialize(this);
            _statesDictionary.Add(stateCopy.GetType(), stateCopy);

            if (!CurrentState)
            {
                CurrentState = stateCopy;
                CurrentState.Enter();
            }
        }
    }

    private void Update()
    {
        CurrentState.Update();
    }

    private void FixedUpdate()
    {
        CurrentState.FixedUpdate();
    }

    public T GetState<T>()
    {
        Type type = typeof(T);
        if (!_statesDictionary.ContainsKey(type))
        {
            throw new NullReferenceException("No State of type: " + type + " found.");
        }

        return (T)Convert.ChangeType(_statesDictionary[type], type);
    }

    public void TransitionToState<T>()
    {

        CurrentState.Exit();
        CurrentState = GetState<T>() as State;
        CurrentState.Enter();

    }

}
