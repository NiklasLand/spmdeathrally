﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    PlayerController pc;
	// Use this for initialization
	void Start () {
        pc = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        pc.InputAxis.x = Input.GetAxisRaw("Horizontal");
        pc.InputAxis.y = Input.GetAxisRaw("Vertical");

        pc.Buttons[0] = Input.GetKey(KeyCode.LeftShift);
        pc.Buttons[1] = Input.GetKey(KeyCode.K);
        pc.Buttons[2] = Input.GetKey(KeyCode.L);
    }
}
