﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnAdudioObectToPool : MonoBehaviour {

    public AudioClip audioClip;

    public void Start()
    {
        GetComponent<AudioSource>().clip = audioClip;
        GetComponent<AudioSource>().Play();
    }
    void Update () {
        if (!GetComponent<AudioSource>().isPlaying)
        {
            ObjectPool.Destroy(this.gameObject);
        }
	}
}
