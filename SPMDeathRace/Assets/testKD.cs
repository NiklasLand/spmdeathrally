﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testKD : MonoBehaviour {

	// Use this for initialization
	void Start () {
        KDTree tree = new KDTree();

        TrackNode a = new TrackNode(4, 14);
        TrackNode b = new TrackNode(8, 3);
        TrackNode c = new TrackNode(22, 5.8f);
        TrackNode d = new TrackNode(11.2f, 34);
        TrackNode e = new TrackNode(15, 10.1f);
        TrackNode f = new TrackNode(6.1f, 2.05f);

        tree.Insert(a);
        tree.Insert(b);
        tree.Insert(c);
        tree.Insert(d);
        tree.Insert(e);
        tree.Insert(f);

        Vector3 point = new Vector3(1, 1, 1);
        Debug.Log(Vector2.Distance(new Vector2(4, 14), point));
        Debug.Log(Vector2.Distance(new Vector2(8, 3), point));

        Debug.Log(tree.NearestNeighbour(point));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
