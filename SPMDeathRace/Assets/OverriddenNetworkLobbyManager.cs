﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class OverriddenNetworkLobbyManager : NetworkLobbyManager {

    private OverriddenNetworkLobbyManager Netman;
    public void Start()
    {
        Netman = FindObjectOfType<OverriddenNetworkLobbyManager>();
        MainMenu mu = FindObjectOfType<MainMenu>();
        mu.Host += Host;
        mu.Join += Join;
    }
    public void Host()
    {
        Netman.StartHost();
    }
    public void Join(string name)
    {
        Netman.networkAddress = name;
        Netman.StartClient();
    }
}
